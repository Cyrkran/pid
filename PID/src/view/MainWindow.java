package view;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import controller.Main;
import controller.Metadados;
import model.Mensagem;

import javax.swing.JButton;

public class MainWindow implements ActionListener{

	private JFrame frame;
	
	private JButton btnAbrirImagem;
	private JButton btnCriarImagem;
	private JButton btnAplicarFiltros;
	
	final Color corBranco = new Color(255, 255, 255);

	Mensagem msg = new Mensagem();

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
					//new Metadados().gerarMetadadosIniciais();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * Construtor padrao que chama o método de inicializacao dos componentes.
	 */
	public MainWindow() {
		initialize();
	}

	
	/**
	 * Inicializa todos os componentes do frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 350, 170);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.getContentPane().setBackground(corBranco);
		
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 434, 21);
		frame.getContentPane().add(menuBar);
		
		JMenu mnNewMenu = new JMenu("Ajuda");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmSobre = new JMenuItem("Manual");
		mnNewMenu.add(mntmSobre);
		
		JMenu mnDesenvolvedores = new JMenu("Cr\u00E9ditos");
		menuBar.add(mnDesenvolvedores);
		
		JMenuItem mntmDesenvolvedores = new JMenuItem("Desenvolvedores");
		mnDesenvolvedores.add(mntmDesenvolvedores);
		
		btnAbrirImagem = new JButton("");
		btnAbrirImagem.setIcon(new ImageIcon(getClass().getResource("/imagens/add72.png")));
		btnAbrirImagem.setToolTipText("Abrir Imagem");
		btnAbrirImagem.setBounds(10, 32, 89, 88);
		frame.getContentPane().add(btnAbrirImagem);
		
		btnCriarImagem = new JButton("");
		btnCriarImagem.setToolTipText("Criar Imagem");
		btnCriarImagem.setBounds(122, 32, 89, 88);
		frame.getContentPane().add(btnCriarImagem);
		btnCriarImagem.setIcon(new ImageIcon(getClass().getResource("/imagens/criar.png")));
		
		btnAplicarFiltros = new JButton("");
		btnAplicarFiltros.setToolTipText("Aplicar Filtros");
		btnAplicarFiltros.setBounds(235, 32, 89, 88);
		btnAplicarFiltros.setIcon(new ImageIcon(getClass().getResource("/imagens/filter.png")));
		frame.getContentPane().add(btnAplicarFiltros);
		
		btnAbrirImagem.addActionListener(this);
		btnCriarImagem.addActionListener(this);
		btnAplicarFiltros.addActionListener(this);
		
		mntmDesenvolvedores.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				msg.desenvolvedores();
			}
		});
		
		mntmSobre.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			//	System.out.println(System.getProperties().get("os.name"));
				try {				
					java.awt.Desktop.getDesktop().open( new File( "documentos/index.html" ) );

				} catch (IOException e1) {
					// TODO Auto-generated catch block
					msg.error(7);
				}
			}
		});

	}
	
	
	/**
	 * Define as ações que devem ser feitas de acordo com o clique do botão.
	 */
	public void actionPerformed(ActionEvent e) {
		Main main = new Main();
		Object botao = e.getSource();
		File file;
		
		if(botao == btnAbrirImagem){
			file = main.escolherImagem();
			if(file != null){
				new ImageWindow(file);
			}
		} else if(botao == btnCriarImagem){
			try {
				PaintWindow window = new PaintWindow();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else if(botao == btnAplicarFiltros){
			file = main.escolherImagem();
			if(file != null){
				new FilterWindow(file);
		//		msg.mensagem(1);
			}
		} 
	};
		
}
