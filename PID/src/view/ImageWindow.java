package view;

import java.io.File;
import java.io.IOException;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;

import controller.GerarHistograma;
import controller.Metadados;
import controller.Ordenacao;
import controller.PrecisaoRevocacao;
import controller.ZoomLabel;
import model.Mensagem;

public class ImageWindow extends JFrame {

	private String[] nomes = new String[0];
	private double ordenado [][] = new double[2][1000];
	
	private JFrame frame = new JFrame();
	private String nome = "";
	private JSlider slider;
	File file = null;
	
	private JLabel lblImagem;
	
	private JTextField tfNome;
	private JTextField tfTipo;
	private JTextField tfTamanho;
	private JTextField tfLocalizacao;
	
	private DefaultListModel<Image> dmlist = new DefaultListModel<Image>();
	private JList<Image> jList = new JList<Image>(dmlist);

	private static final Color corBranco = new Color(255, 255, 255);
	private static final Color corCinza = new Color(245, 245, 245);
	private static final Color corPreto = new Color(0, 0, 0);
	
	public static final String PATH = System.getProperty("user.dir") + System.getProperty("file.separator") + "bin" + 
			System.getProperty("file.separator") + "imagens" + System.getProperty("file.separator") + 
			"base_de_imagens" + System.getProperty("file.separator");
	
	private static final String PATH_HIST = System.getProperty("user.dir") + System.getProperty("file.separator") + "bin" + 
			System.getProperty("file.separator") + "imagens" +System.getProperty("file.separator") + "histogramas" + 
			System.getProperty("file.separator");
	
	private static final String PATH_METADADOS = System.getProperty("user.dir") + System.getProperty("file.separator") + "bin" + 
			System.getProperty("file.separator") + "metadados" +System.getProperty("file.separator");

	private static final String EXT = ".jpg";
	
	final ZoomLabel zoomLabel = new ZoomLabel();
	JComboBox combo = null;
	
	BufferedImage bi = null;
	Mensagem msg = new Mensagem();
	
	GerarHistograma histograma = new GerarHistograma();
		
	private boolean ordem = false;
	
	private JFreeChart pxRChart;
	
	boolean imagemBusca = false;
		
	
	/**
	 * Construtor padrao que chama o metodo de inicializacao dos componentes.
	 */
	public ImageWindow() {
		initialize();
	}
	
	
	/**
	 * Construtor que chama o metodo de inicializacao dos componentes e o metodo para setar a imagem selecionada.
	 * @param imagem - Objeto do tipo File.
	 */
	public ImageWindow(File imagem) {			
		initialize();
		abrirImagem(imagem);
		file = imagem;
	}

	
	/**
	 * Inicializa todos os componentes do frame.
	 */
	private void initialize() {	
		frame.setBounds(50, 0, 1152, 864);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
		frame.getContentPane().setLayout(null);
		frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		frame.getContentPane().setBackground(corBranco);
		
		lblImagem = new JLabel("");
		lblImagem.setBounds(60, 10, 384, 256);
		
		JLabel lblPropriedades = new JLabel("Propriedades");
		lblPropriedades.setBounds(100, 250, 100, 50);
			
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(25, 280, 100, 50);
		
		tfNome = new JTextField("");
		tfNome.setBounds(80, 280, 215, 50);
		tfNome.setEditable(true);
		tfNome.setBorder(null);
		tfNome.setBackground(null);
		
		JLabel lblTipo = new JLabel("Tipo:");
		lblTipo.setBounds(25, 320, 100, 50);
		
		tfTipo = new JTextField("");
		tfTipo.setBounds(80, 320, 215, 50);
		tfTipo.setEditable(false);
		tfTipo.setBorder(null);
		tfTipo.setBackground(null);
		
		JLabel lblTamanho = new JLabel("Tam.:");
		lblTamanho.setBounds(25, 360, 100, 50);
		
		tfTamanho = new JTextField("");
		tfTamanho.setBounds(80, 360, 215, 50);
		tfTamanho.setEditable(false);
		tfTamanho.setBorder(null);
		tfTamanho.setBackground(null);
		
		JLabel lblLocalizacao = new JLabel("Local:");
		lblLocalizacao.setBounds(25, 400, 100, 50);
		
		tfLocalizacao = new JTextField("");
		tfLocalizacao.setBounds(80, 400, 215, 50);
		tfLocalizacao.setEditable(false);
		tfLocalizacao.setBorder(null);
		tfLocalizacao.setBackground(null);
		
		JLabel lblOutros = new JLabel("Outros");
		lblOutros.setBounds(120, 5, 100, 50);
		lblOutros.setVisible(true);
		
		JButton btnGrafico = new JButton(new ImageIcon(getClass().getResource("/imagens/grafico.png")));
		btnGrafico.setText("Precisao x Revocacao");
		btnGrafico.setBounds(20, 50, 200, 40);
		btnGrafico.setBackground(null);
		btnGrafico.setBorder(null);
		
		JButton btnSegmentacao = new JButton(new ImageIcon(getClass().getResource("/imagens/segmentacao.png")));
		btnSegmentacao.setText("Segmentacao");
		btnSegmentacao.setBounds(20, 100, 147, 40);
		btnSegmentacao.setBackground(null);
		btnSegmentacao.setBorder(null);
		btnSegmentacao.setVisible(false);
			
		JPanel pnlPropriedades = new JPanel();
		pnlPropriedades.setBounds(0, 0, 330, 481);
		pnlPropriedades.setLayout(null);
		pnlPropriedades.setBackground(corCinza);
		pnlPropriedades.setBorder(new LineBorder(corPreto));
		pnlPropriedades.add(lblImagem);
		pnlPropriedades.add(lblPropriedades);
		pnlPropriedades.add(lblNome);
		pnlPropriedades.add(tfNome);
		pnlPropriedades.add(lblTipo);
		pnlPropriedades.add(tfTipo);
		pnlPropriedades.add(lblTamanho);
		pnlPropriedades.add(tfTamanho);
		pnlPropriedades.add(lblLocalizacao);
		pnlPropriedades.add(tfLocalizacao);
		pnlPropriedades.setVisible(true);
		frame.getContentPane().add(pnlPropriedades);
		
		JPanel pnlOpcoes = new JPanel();
		pnlOpcoes.setBounds(0, 480, 330, 300);
		pnlOpcoes.setLayout(null);
		pnlOpcoes.setBackground(corCinza);
		pnlOpcoes.setBorder(new LineBorder(corPreto));
		pnlOpcoes.add(lblOutros);
		pnlOpcoes.add(btnGrafico);
		pnlOpcoes.add(btnSegmentacao);
		pnlOpcoes.setVisible(true);
	//	frame.getContentPane().add(pnlOpcoes);
		
		JTextField tfLinhaH = new JTextField("");
		tfLinhaH.setBounds(0, 480, frame.getWidth(), 1);
		tfLinhaH.setBorder(new LineBorder(corPreto));
		tfLinhaH.setEditable(false);
		tfLinhaH.setBackground(corPreto);
		
		combo = new JComboBox<String>(new String[]{"5","10", "15", "30", "100", "1000"});
		combo.setBounds(1220, 490, 60, 20);
		
		jList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		jList.setVisibleRowCount(1);
		jList.setBorder(new LineBorder(corBranco));

		JScrollPane scroll = new JScrollPane(jList);
		scroll.setBounds(120, 500, 1050, 210);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
						
		JPanel pnlImagens = new JPanel();
		pnlImagens.add(scroll);
		
		JLabel labelHistograma = new JLabel();
		labelHistograma.setVisible(true);		

		JPanel pnlHistograma = new JPanel();
		pnlHistograma.add(labelHistograma);	
		pnlHistograma.setBounds(455, 35, 710, 420);
		
		//mostrar imagem de busca no painel quando clicar
		lblImagem.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				imagemBusca = true;
				zoom(bi);
				
				if(histograma.histogramaExiste(tfNome.getText()) == false){
					histograma.gerarHistograma(tfNome.getText());
					labelHistograma.setIcon(new ImageIcon((PATH_HIST + tfNome.getText())));
				} else{
					histograma.gerarHistograma(tfNome.getText());
					labelHistograma.setIcon(new ImageIcon((PATH_HIST + tfNome.getText())));
				}
				
				jList.setSelectedIndex(-1);				
			}
		});
		
		
		slider = new JSlider(0,1000,100);
		slider.setPaintLabels(true);
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int val = ((JSlider) e.getSource()).getValue();
				zoomLabel.setScale(val * .01f, val * .01f);
			}
		});
		
		
		final JInternalFrame frameInterno = new JInternalFrame();
		frameInterno.add(new JScrollPane(zoomLabel));
		frameInterno.setBounds(455, 35, 710, 420);
		frameInterno.add(slider, java.awt.BorderLayout.SOUTH);
		frameInterno.putClientProperty("JInternalFrame.isPalette", Boolean.TRUE);
		frameInterno.getContentPane().setBackground(corBranco);
		frameInterno.setVisible(true);
		
		JPanel pnlImagem = new JPanel();
		pnlImagem.add(frameInterno);
		
		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.add(pnlImagem, "Imagem");  
		tabbedPane.add(pnlHistograma, "Histograma");
		tabbedPane.setBounds(450, 10, 720, 450);
    
		JMenuBar barra = new JMenuBar();
		frame.setJMenuBar(barra);
		
		JMenu menuBusca = new JMenu("Busca");
		barra.add(menuBusca);	
		
		Metadados mt = new Metadados();
		
		JMenuItem itemRGB = new JMenuItem("RGB");
		itemRGB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				char distancia = opcaoDistancia();
				File file = new File(PATH_METADADOS+nome+"RGB" + distancia+".txt");
				if(!file.exists()){
					Ordenacao o = new Ordenacao();
					ordenado = o.ordenarImagens(bi, distancia, "RGB");
					mt.gerarMetadados(file, ordenado);
				}else{
					ordenado = mt.lerArquivo(file);
				}
				
				ordem = true;
				reorganizarLista(5);
			}
		});
		
		
		JMenuItem itemRGBN = new JMenuItem("RGB Normalizado");
		itemRGBN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				char distancia = opcaoDistancia();
				File file = new File(PATH_METADADOS+nome+"RGBN.txt");
				if(!file.exists()){
					Ordenacao o = new Ordenacao();
					ordenado = o.ordenarImagens(bi, distancia, "RGBN");
					mt.gerarMetadados(file, ordenado);
				}else{
					ordenado = mt.lerArquivo(file);
				}
				ordem = true;
				reorganizarLista(5);
			}
		});
		
		
		JMenuItem itemYUV = new JMenuItem("YUV");
		itemYUV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				char distancia = opcaoDistancia();
				File file = new File(PATH_METADADOS+nome+"YUV.txt");
				if(!file.exists()){
					Ordenacao o = new Ordenacao();
					ordenado = o.ordenarImagens(bi, distancia, "YUV");
					mt.gerarMetadados(file, ordenado);
				}else{
					ordenado = mt.lerArquivo(file);
				}
				ordem = true;
				reorganizarLista(5); 
			}
		});
		
		
		JMenuItem itemYUVN = new JMenuItem("YUV Normalizado");
		itemYUVN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				char distancia = opcaoDistancia();
				File file = new File(PATH_METADADOS+nome+"YUVN.txt");
				if(!file.exists()){
					Ordenacao o = new Ordenacao();
					ordenado = o.ordenarImagens(bi, distancia, "YUVN");
					mt.gerarMetadados(file, ordenado);
				}else{
					ordenado = mt.lerArquivo(file);
				}
				ordem = true;
				reorganizarLista(5); 
			}
		});
		
		JMenuItem itemHSV = new JMenuItem("HSV");
		itemHSV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				char distancia = opcaoDistancia();
				File file = new File(PATH_METADADOS+nome+"HSV.txt");
				if(!file.exists()){
					Ordenacao o = new Ordenacao();
					ordenado = o.ordenarImagens(bi, distancia, "HSV");
					mt.gerarMetadados(file, ordenado);
				}else{
					ordenado = mt.lerArquivo(file);
				}
				reorganizarLista(5); 
			}
		});
		
		JMenuItem itemHSVN = new JMenuItem("HSV Normalizado");
		itemHSVN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				char distancia = opcaoDistancia();
				File file = new File(PATH_METADADOS+nome+"HSVN.txt");
				if(!file.exists()){
					Ordenacao o = new Ordenacao();
					ordenado = o.ordenarImagens(bi, distancia, "HSVN");
					mt.gerarMetadados(file, ordenado);
				}else{
					ordenado = mt.lerArquivo(file);
				}
				ordem = true;
				reorganizarLista(5); 
			}
		});
		
		menuBusca.add(itemRGB);
		menuBusca.add(itemRGBN);
		menuBusca.add(itemYUV);
		menuBusca.add(itemYUVN);
		menuBusca.add(itemHSV);
		menuBusca.add(itemHSVN);
		
		JMenu menuGrafico = new JMenu("Precisão x Revocação");
		barra.add(menuGrafico);
		
		JMenuItem itemGrafico = new JMenuItem("Abrir                          "); //não olhe para essa linha :P
		itemGrafico.setSize(menuGrafico.getWidth(), menuGrafico.getHeight());
		itemGrafico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pxRChart = ChartFactory.createLineChart("Precisao X Revocacao", "Precisao", "Revocacao", pxRDataset());
				JFrame graphFrame = new JFrame("Grafico"); 
				ChartPanel graphPanel = new ChartPanel(pxRChart); 
				graphFrame.add(graphPanel);
				graphFrame.setBounds(100, 100, 400, 400); 
				graphFrame.setVisible(true);
			}
		});
		
		menuGrafico.add(itemGrafico);

		barra.add(menuBusca);
		barra.add(menuGrafico);
		
        
		/** 
		 * Muda o estado dependendo da aba selecionada.
		 */
		tabbedPane.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JTabbedPane sourceTabbedPane = (JTabbedPane) e.getSource();
				
				if(sourceTabbedPane.getSelectedIndex() == 0){
					frameInterno.setVisible(true);
				} else if(sourceTabbedPane.getSelectedIndex() == 1){
					frameInterno.setVisible(false);
					labelHistograma.setVisible(true);
					if(jList.isSelectionEmpty() && imagemBusca == false){
						msg.mensagem(0);
					} else{
						if(imagemBusca == false){
							if(histograma.histogramaExiste(nomes[jList.getSelectedIndex()]) == false){
								histograma.gerarHistograma(nomes[jList.getSelectedIndex()]);
								labelHistograma.setIcon(new ImageIcon((PATH_HIST + nomes[jList.getSelectedIndex()])));
							} else{
								histograma.gerarHistograma(nomes[jList.getSelectedIndex()]);
								labelHistograma.setIcon(new ImageIcon((PATH_HIST + nomes[jList.getSelectedIndex()])));
							}
						}
					}
				}			
			}
		});

		
		
		/**
		 * Envia o valor selecionado a cada nova selecao na lista de imagens.
		 */
		jList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				//pegar imagem selecionada
				if(jList.getSelectedIndex() >= 0){
					imagemBusca = false;
					selecionado(PATH + nomes[jList.getSelectedIndex()]);	
					if(tabbedPane.getSelectedIndex() == 1){
						frameInterno.setVisible(false);
						if(jList.isSelectionEmpty()){
							msg.mensagem(0);
						} else{
							if(histograma.histogramaExiste(nomes[jList.getSelectedIndex()]) == false){
								histograma.gerarHistograma(nomes[jList.getSelectedIndex()]);
								labelHistograma.setIcon(new ImageIcon((PATH_HIST + nomes[jList.getSelectedIndex()])));
							} else{
								histograma.gerarHistograma(nomes[jList.getSelectedIndex()]);
								labelHistograma.setIcon(new ImageIcon((PATH_HIST + nomes[jList.getSelectedIndex()])));
							}
						}
					}
				}
			}
		});
		
		
		/**
		 * Envia o valor selecionado a cada nova selecao no combobox.
		 */
		combo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				limparLista();
				int s = Integer.parseInt(combo.getSelectedItem().toString());
				if(ordem == false){
					preencherLista(s);
				}else{
					reorganizarLista(s);
				}
			}
		});
		
		
		lblImagem.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {

			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				atualizarImagem();
				
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {

			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
			}
		});	

		
		/** 
		 * Define as acoes de zoom com ~a rodinha do mouse~ para o frame.
		 */
		frameInterno.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				//se < 0 sobe a tela
				if(e.getWheelRotation() < 0){
					slider.setValue(slider.getValue()+10);
				} else if(e.getWheelRotation() > 0){
					slider.setValue(slider.getValue()-10);
				}	
			}
		});
		
		
		/** 
		 * Define acoes de zoom com ~a rodinha do mouse~ para o label.
		 */
		zoomLabel.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				//se < 0 sobe a tela
				if(e.getWheelRotation() < 0){
					slider.setValue(slider.getValue()+10);
				} else if(e.getWheelRotation() > 0){
					slider.setValue(slider.getValue()-10);
				}
			}
		});
		
		
		/**
		 * Impedir que o frame interno seja movido.
		 */
		frameInterno.addComponentListener(new ComponentListener() {
			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub
			}
		
			@Override
			public void componentResized(ComponentEvent e) {
				// TODO Auto-generated method stub	
			}
		
			@Override
			public void componentMoved(ComponentEvent e) {
				//impedir de mover
				frameInterno.setLocation(455, 35);
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
			}
		});
		
		//entitulando grafico


		
		btnGrafico.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) {
				JFrame graphFrame = new JFrame("Grafico"); 
				ChartPanel graphPanel = new ChartPanel(pxRChart); 
				graphFrame.add(graphPanel);
				graphFrame.setBounds(100, 100, 400, 400); 
				graphFrame.setVisible(true);
			} 
		});
			

		frame.add(frameInterno);
		frame.add(tfLinhaH);
		frame.add(scroll);
		frame.add(combo);
		frame.add(tabbedPane);
		tfNome.requestFocus();
	}


	/**	
	 * Metodo que verifica qual metrica de distancia sera usada no calculo.
	 * @return distancia - Char indicando a metrica a ser utilizada.
	 * c = cosseno, e = euclidiana, m = manhattan, x = xadrez.
	 */
	public char opcaoDistancia(){    
		char distancia = ' ';
		ButtonGroup grupo = new ButtonGroup();
        
		String texto = "Escolha a medida de similaridade: ";
		
		JRadioButton rbCosseno = new JRadioButton("Cosseno");
		JRadioButton rbEuclidiana = new JRadioButton("Euclidiana");
		JRadioButton rbManhattan = new JRadioButton("Manhattan");
		JRadioButton rbXadrez = new JRadioButton("Xadrez");
		
		rbEuclidiana.setSelected(true);

		grupo.add(rbCosseno);
		grupo.add(rbEuclidiana);
		grupo.add(rbManhattan);
		grupo.add(rbXadrez);
		
		Object [] array = {texto, rbCosseno, rbEuclidiana, rbManhattan, rbXadrez};
		int selecao = JOptionPane.showConfirmDialog(null, array,"",JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE);   
		
		if(selecao == 0){
			if(rbCosseno.isSelected()){
				distancia = 'c';
			} else if(rbEuclidiana.isSelected()){
				distancia = 'e';
			} else if(rbManhattan.isSelected()){
				distancia = 'm';
			} else if(rbXadrez.isSelected()){
				distancia = 'x';
			}
		}
		
		return distancia;
	}
	
	
	/**
	 * Gerar grafico precisao x revocacao.
	 * @return dataset - Conjunto de dados.
	 */
	private DefaultCategoryDataset pxRDataset(){
		
		PrecisaoRevocacao pr = new PrecisaoRevocacao(file, Integer.parseInt(combo.getSelectedItem().toString()), nomes);
		return pr.plotarGrafico();
	}
		
	
	/** 
	 * Coloca a imagem selecionada de forma reduzida no labelImagem.
	 */
	public void atualizarImagem() {
        if (lblImagem.getSize().width == 0 || lblImagem.getSize().height == 0) 
        	return;
        
        BufferedImage aux = new BufferedImage(200, 200, bi.getType()); //cria um buffer auxiliar com o tamanho desejado
        Graphics2D g = aux.createGraphics(); //pega a classe graphics do aux para edicao
        AffineTransform at = AffineTransform.getScaleInstance((double) 200 / bi.getWidth(), 
        					(double) 200 / bi.getHeight()); //cria a transformacao
        g.drawRenderedImage(bi, at); //pinta e transforma a imagem real no auxiliar
        lblImagem.setIcon(new ImageIcon(aux)); // seta no jlabel
        
    }
	
    
	/**
	 * Chama o metodo para setar a imagem nos componentes e suas propriedades.
	 * @param file - Objeto do tipo File.
	 */
    public void abrirImagem(File file){
        try {
            bi = ImageIO.read(file); //carrega a imagem real num buffer
            nome = file.getName();
            atualizarImagem();
        } catch (IOException ex) {
        	msg.error(1);
        }
        propriedades(file);
        
        preencherLista(5);
		ordem = false;
	}
    
    
    /**
     * Limpar toda a lista.
     */
    public void limparLista(){
    	dmlist.clear(); 
    	nomes = new String [0];
    }
    
    
    /**
     * Pega o nome do objeto selecionado, busca e adiciona no frameInterno.
     * @param image - String com caminho da imagem.
     */
    public void selecionado(String image){
    	System.out.println(image);
    	ImageIcon icon = new ImageIcon((image));
    	zoom(icon.getImage());
    }
	
	
	/**
     * Reorganiza a lista horizontal com as imagens da base de dados usando a como criterio a menor distancia.
     * @param quant - Inteiro que indica quantos itens serao mostrados.
     */
	public void reorganizarLista(int quant){
		BufferedImage aux = null;
		Image imgMenor = null;	
		
		limparLista();
		
		nomes = new String[quant];

		for(int i = 0; i < quant; i++){
			try {
				nomes[i] = (int)ordenado[0][i] + EXT;
				aux = ImageIO.read(new File (PATH + String.valueOf((int)ordenado[0][i]) + EXT));
			} catch (IOException e) {
				msg.error(3);
				System.exit(0);
			}
			
			imgMenor = aux.getScaledInstance(200, 200, 0);
			 				
			dmlist.addElement(imgMenor);
			jList.setCellRenderer(new ListaRenderer());
		}
	}
	
	
	/**
     * Preenche a lista horizontal com as imagens da base de dados.
     * @param quant - Inteiro que indica quantos itens serao mostrados.
     */
	public void preencherLista(int quant){
		BufferedImage aux = null;
		Image imgMenor = null;
		nomes = new String[quant];
		
		for(int i = 0; i < quant; i++){
			try {
				nomes[i] = i + EXT;
				
				aux = ImageIO.read(new File(PATH + String.valueOf(i) + EXT));
				
			} catch (IOException e) {
				msg.error(3);
				System.exit(0);
			}
			
			imgMenor = aux.getScaledInstance(200, 200, 0);
			 					
			dmlist.addElement(imgMenor);
			jList.setCellRenderer(new ListaRenderer());
		}
	}

	
	/**
	 * Seta a imagem selecionada no frame interno e inicializa as funcões de zoom.
	 * @param imagem - Objeto do tipo Image.
	 */
	public void zoom(Image imagem){
		zoomLabel.setScale(1.0f, 1.0f);
		slider.setValue(100);

        try{
            zoomLabel.setImage(imagem);
            zoomLabel.setMinimumSize(new Dimension(800, 500));
        }catch (Exception e) {
        	msg.error(2);
            System.exit(0);
        }
	}
	
	
	/**
	 * Método que seta as propriedades da imagem selecionada.
	 * @param imagem - Objeto do tipo File a ser utilizado.
	 */
	public void propriedades(File imagem){
		tfNome.setText(imagem.getName());
		tfTipo.setText(getExtension(imagem));
		tfTamanho.setText(String.valueOf(imagem.length() + " bits"));
		tfLocalizacao.setText(imagem.getParent());
	}

	
	/**
	 * Funcao que retorna a extensao do arquivo passado por parametro.
	 * @param f - Objeto do tipo File.
	 * @return ext - String resultado.
	 */
	public String getExtension(File f) {
	        String ext = null;
	        String s = f.getName();
	        int i = s.lastIndexOf('.');

	        if (i > 0 &&  i < s.length() - 1) {
	            ext = s.substring(i+1).toLowerCase();
	        }
	        return ext;
	}
	
}//end class ImageWindow


/**
 * Classe para manipulacao da lista de imagens horizontal.
 */
class ListaRenderer extends JLabel implements ListCellRenderer {
    private Image img = null;
    
	public ListaRenderer() {
      setOpaque(true);
    }
	
	/**
	 * Metodo que manipula a celula, item individual da lista.
	 */
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
    boolean cellHasFocus) {
    	if (value != null){
    		img = (Image) value;
    		setIcon(new ImageIcon(img));
    	} 
    	
    	
    	if (isSelected) {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        } 
    	
    	
    	if (cellHasFocus){
    		setBorder(UIManager.getBorder("List.focusCellHighlightBorder"));
    		setHorizontalAlignment(CENTER);
    	} else{
    		setBorder(new EmptyBorder(1, 1, 1, 10));
    		setHorizontalAlignment(CENTER);
    	} 
    	
    	return this;
    }
}
