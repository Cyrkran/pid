package view;

import java.io.File;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;



import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import controller.Filter;
import controller.ZoomLabel;
import model.Mensagem;

public class FilterWindow extends JFrame {
	String limiar = "";
	
	String[] nomes = new String[0];
	private JFrame frame;
	private JSlider slider;
	
	private JLabel lblImagem;	
	private JLabel lblFiltros;
	
	private JRadioButton rbMaximo;
	private JRadioButton rbMinimo;
	private JRadioButton rbMediana;
	private JRadioButton rbMedia;
	private JRadioButton rbBinarizacao;
	private JRadioButton rbMonocromatica;
	private JRadioButton rbFourier;
	private JRadioButton rbInversaFourier;
	private JRadioButton rbBorda;
	private JRadioButton rbPrewitt;
	private JRadioButton rbRoberts;
	private JRadioButton rbSobel;
	private JRadioButton rbNegativo;
	
	private JPanel pnlFiltros;
	
	BufferedImage bi = null;
	
	Mensagem msg = new Mensagem();
	
	final Color corBranco = new Color(255, 255, 255);
	final Color corCinza = new Color(245, 245, 245);
	final Color corPreto = new Color(0, 0, 0);
	
	Filter filter = null;
	final ZoomLabel zoomLabel = new ZoomLabel();
	File imagemFile = null;

	
	/**
	 * Construtor padrão que chama o método de inicialização dos componentes.
	 */
	public FilterWindow() {
		initialize();
	}
	
	
	/**
	 * Construtor que chama o método de inicialização dos componentes e o método para setar a imagem selecionada.
	 * @param imagem - Objeto do tipo File.
	 */
	public FilterWindow(File imagem) {
		initialize();
		imagemFile = imagem;
		abrirImagem(imagem);		
	}

	
	/**
	 * Inicializa todos os componentes do frame.
	 */
	private void initialize() {	
		frame = new JFrame();
		frame.setBounds(50, 50, 1152, 864);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
		frame.getContentPane().setLayout(null);
		frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		frame.getContentPane().setBackground(corBranco);
		
		lblImagem = new JLabel("");
		lblImagem.setBounds(60, 20, 384, 256);
			
		lblFiltros = new JLabel("Filtros");
		lblFiltros.setBounds(110, 252, 100, 50);
			
		rbMaximo = new JRadioButton("Maximo");
		//jcMaximo.setEnabled(false);
		rbMaximo.setBounds(25, 285, 300, 32);
		rbMaximo.setBackground(corCinza);
		
		rbMinimo = new JRadioButton("Minimo");
		//jcMinimo.setEnabled(false);
		rbMinimo.setBounds(25, 320, 300, 32);
		rbMinimo.setBackground(corCinza);
		
		rbMediana = new JRadioButton("Mediana");
		rbMediana.setEnabled(true);
		rbMediana.setBounds(25, 355, 300, 32);
		rbMediana.setBackground(corCinza);
		
		rbMedia = new JRadioButton("Media");
		//jcMedia.setEnabled(false);
		rbMedia.setBounds(25, 390, 300, 32);
		rbMedia.setBackground(corCinza);
		
		rbBinarizacao = new JRadioButton("Transformacao de Binarizacao");
		rbBinarizacao.setBounds(25, 425, 300, 32);
		rbBinarizacao.setBackground(corCinza);
		
		rbMonocromatica = new JRadioButton("Transformacao Monocromatica");
		rbMonocromatica.setBounds(25, 460, 300, 32);
		rbMonocromatica.setBackground(corCinza);
		rbMonocromatica.setSelected(true);
		
		rbFourier = new JRadioButton("Transformacao de Fourier");
		//rbFourier.setEnabled(false);
		rbFourier.setBounds(25, 495, 300, 32);
		rbFourier.setBackground(corCinza);
		
		rbInversaFourier = new JRadioButton("Inversa de Fourier");
		rbInversaFourier.setBounds(25, 530, 300, 32);
		rbInversaFourier.setBackground(corCinza);
		
		rbBorda = new JRadioButton("Bordas");
		rbBorda.setBackground(new Color(245, 245, 245));
		rbBorda.setBounds(25, 565, 300, 32);
		
		rbPrewitt = new JRadioButton("Prewitt");
		rbPrewitt.setVisible(false);
		rbPrewitt.setBounds(25, 570, 109, 23);
		rbPrewitt.setBackground(corCinza);
		
		rbRoberts = new JRadioButton("Roberts");
		rbRoberts.setVisible(false);
		rbRoberts.setBounds(25, 600, 109, 23);
		rbRoberts.setBackground(corCinza);
		
		rbSobel = new JRadioButton("Sobel");
		rbSobel.setVisible(false);
		rbSobel.setBounds(25, 630, 109, 23);
		rbSobel.setBackground(corCinza);
		
		rbNegativo = new JRadioButton("Negativo");
		rbPrewitt.setVisible(true);
		rbNegativo.setBounds(25, 595, 109, 23);
		rbNegativo.setBackground(corCinza);
		
		ButtonGroup group = new ButtonGroup();
		group.add(rbMaximo);
		group.add(rbMinimo);
		group.add(rbMediana);
		group.add(rbBorda);
		group.add(rbMedia);
		group.add(rbBinarizacao);
		group.add(rbMonocromatica);
		group.add(rbFourier);
		group.add(rbInversaFourier);
		group.add(rbPrewitt);
		group.add(rbRoberts);
		group.add(rbSobel);
		group.add(rbNegativo);
				
		pnlFiltros = new JPanel();
		pnlFiltros.setBounds(0, 0, 330, 800);
		pnlFiltros.setLayout(null);
		pnlFiltros.setBackground(corCinza);
		pnlFiltros.setBorder(new LineBorder(corPreto));
		pnlFiltros.add(lblImagem);
		pnlFiltros.add(lblFiltros);
		pnlFiltros.add(rbMaximo);
		pnlFiltros.add(rbMinimo);
		pnlFiltros.add(rbMediana);
		pnlFiltros.add(rbMedia);
		pnlFiltros.add(rbBinarizacao);
		pnlFiltros.add(rbMonocromatica);
		pnlFiltros.add(rbFourier);
		pnlFiltros.add(rbInversaFourier);
		pnlFiltros.add(rbBorda);
		pnlFiltros.add(rbPrewitt);
		pnlFiltros.add(rbRoberts);
		pnlFiltros.add(rbSobel);
		pnlFiltros.add(rbNegativo);

		pnlFiltros.setVisible(true);
		
		frame.getContentPane().add(pnlFiltros);

		
		rbBorda.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				rbBorda.setVisible(false);
				rbPrewitt.setVisible(true);
				rbPrewitt.setSelected(true);
				rbRoberts.setVisible(true);
				rbSobel.setVisible(true);
				rbNegativo.setBounds(25, 660, 109, 23);
			}
		});
		
		
		JButton btnAplicarFiltros = new JButton("<html>Aplicar Filtros</html>");
			
		
		rbBinarizacao.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(rbBinarizacao.isSelected()){
					limiar = "";
					System.out.println("VAZIO "+limiar);

					limiar = msg.entrada(0); 	
					if(limiar != null && !limiar.equals("")){
						System.out.println(limiar);
						btnAplicarFiltros.doClick();
					}
				}		
			}
		});
		
		
		btnAplicarFiltros.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Filter filter = new Filter(imagemFile);
				
				if(rbMaximo.isSelected()){
					String nome = filter.filtroMaximo(imagemFile);
					mostrarImagemFiltrada(nome);				
				} else if(rbMinimo.isSelected()){
					String nome = filter.filtroMinimo(imagemFile);
					mostrarImagemFiltrada(nome);				
				} else if(rbPrewitt.isSelected()){
					String nome = filter.filtroBordaPrewitt(imagemFile);
					mostrarImagemFiltrada(nome);
				} else if(rbRoberts.isSelected()){
					String nome = filter.filtroBordaRoberts(imagemFile);
					mostrarImagemFiltrada(nome);
				} else if(rbSobel.isSelected()){
					String nome = filter.filtroBordaSobel(imagemFile);
					mostrarImagemFiltrada(nome);
				} else if(rbMediana.isSelected()){
					String nome = filter.filtroMediana(imagemFile);
					mostrarImagemFiltrada(nome);
				} else if(rbMedia.isSelected()){
					String nome = filter.filtroMedia(imagemFile);
					mostrarImagemFiltrada(nome);
				} else if(rbBinarizacao.isSelected()){
					if(limiar != null && !limiar.equals("")){
						String nome = filter.aplicarFiltroBinarizacao(imagemFile, Integer.parseInt(limiar)); 
						mostrarImagemFiltrada(nome);
					}
				} else if(rbMonocromatica.isSelected()){
		            String nome = filter.filtroMonocromatico(imagemFile);
		            mostrarImagemFiltrada(nome);
		        } else if(rbFourier.isSelected()){
					String nome = filter.aplicarTransformadaFourier(imagemFile);
					mostrarImagemFiltrada(nome);
				} else if(rbInversaFourier.isSelected()){
					String nome = filter.aplicarInversaFourier(imagemFile);
					mostrarImagemFiltrada(nome);
				} else if(rbNegativo.isSelected()){
					String nome = filter.aplicarNegativo(imagemFile);
					mostrarImagemFiltrada(nome);
				}
			}
		});
		
		btnAplicarFiltros.setBounds(209, 617, 110, 40);
		pnlFiltros.add(btnAplicarFiltros);
			
		
		lblImagem.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {}
			
			@Override
			public void componentResized(ComponentEvent e) {
				atualizarImagem();
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {}
			
			@Override
			public void componentHidden(ComponentEvent e) {}
		});	
		
		
		//mostrar imagem de busca no painel quando clicar		
		lblImagem.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				zoom(bi);				
			}
		});
	
		final JInternalFrame frameInterno = new JInternalFrame();
		frameInterno.getContentPane().add(new JScrollPane(zoomLabel));
		
		slider = new JSlider(0,1000,100);
		slider.setPaintLabels(true);
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int val = ((JSlider) e.getSource()).getValue();
				zoomLabel.setScale(val * .01f, val * .01f);
			}
		});
		
		
		/** 
		 * Define as ações de zoom com ~a rodinha do mouse~ para o frame.
		 */
		frameInterno.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				//se < 0 sobe a tela
				if(e.getWheelRotation() < 0){
					slider.setValue(slider.getValue()+10);
				} else if(e.getWheelRotation() > 0){
					slider.setValue(slider.getValue()-10);
				}	
			}
		});
		
		
		/** 
		 * Define as ações de zoom com ~a rodinha do mouse~ para o label.
		 */
		zoomLabel.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				//se < 0 sobe a tela
				if(e.getWheelRotation() < 0){
					slider.setValue(slider.getValue()+10);
				} else if(e.getWheelRotation() > 0){
					slider.setValue(slider.getValue()-10);
				}
			}
		});

		
		frameInterno.setBounds(385, 40, 850, 655);
		frameInterno.getContentPane().add(slider, java.awt.BorderLayout.SOUTH);
		frameInterno.putClientProperty("JInternalFrame.isPalette", Boolean.TRUE);
		frameInterno.getContentPane().setBackground(corBranco);
		frameInterno.setVisible(true);
		
		
		/**
		 * Impedir que o frame interno seja movido.
		 */
		frameInterno.addComponentListener(new ComponentListener() {
			@Override
			public void componentShown(ComponentEvent e) {}
		
			@Override
			public void componentResized(ComponentEvent e) {}
		
			@Override
			public void componentMoved(ComponentEvent e) {
				//impedir de mover
				frameInterno.setLocation(500, 40);
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {}
		});
     
			        
		frame.getContentPane().add(frameInterno);
	}
	
	
	/** 
	 * Coloca a imagem selecionada de forma reduzida no labelImagem.
	 */
	public void atualizarImagem() {
        if (lblImagem.getSize().width == 0 || lblImagem.getSize().height == 0) 
        	return;
        
        BufferedImage aux = new BufferedImage(200, 200, bi.getType()); //cria um buffer auxiliar com o tamanho desejado
        Graphics2D g = aux.createGraphics(); //pega a classe graphics do aux para edicao
        AffineTransform at = AffineTransform.getScaleInstance((double) 200 / bi.getWidth(), 
        					(double) 200 / bi.getHeight()); //cria a transformacao
        g.drawRenderedImage(bi, at); //pinta e transforma a imagem real no auxiliar
        lblImagem.setIcon(new ImageIcon(aux)); // seta no jlabel        
    }
	
    
	/**
	 * Chama o método para setar a imagem nos componentes.
	 * @param file - Objeto do tipo File.
	 */
    public void abrirImagem(File file){
        try{
            bi = ImageIO.read(file); //carrega a imagem real num buffer            
            atualizarImagem();
            ImageIcon icon = new ImageIcon(file.getPath());
        	zoom(icon.getImage());
        } catch(Exception ex) {
        	msg.error(1);
        }
	}
    
	/**
	 * Passa para o metodo zoom a imagem filtrada a ser setada no frame interno.
	 * @param nome - String que indica o nome da imagem filtrada.
	 */
    public void mostrarImagemFiltrada(String nome){
        try{
        	ImageIcon icon = null;
            icon = new ImageIcon(System.getProperty("user.dir") + System.getProperty("file.separator") + "bin" + 
					System.getProperty("file.separator") + "imagens" +System.getProperty("file.separator") + "filtros" + 
            		System.getProperty("file.separator") + nome);
        	zoom(icon.getImage());
        } catch(Exception ex) {
        	msg.error(1);
        }
	}
    	
    
	/** Seta a imagem selecionada no frame interno e inicializa as funções de zoom.
	 * @param imagem - Objeto do tipo Image.
	 */
	public void zoom(Image imagem){
		zoomLabel.setScale(1.0f, 1.0f);
		slider.setValue(100);

        try{
            zoomLabel.setImage(imagem);
            zoomLabel.setMinimumSize(new Dimension(800, 500));
        }catch (Exception e) {
        	msg.error(2);
            System.exit(0);
        }
	}
}//end class FilterWindow
