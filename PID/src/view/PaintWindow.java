package view;


import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import model.PadDraw;

import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class PaintWindow {

	private JFrame frame;
	public JRadioButton rdbtnTriangulo = new JRadioButton("Arredondado");
	public JRadioButton rdbtnRetangulo = new JRadioButton("Retangulo");
	public JRadioButton rdbtnLinha = new JRadioButton("Linha");
	public JRadioButton rdbtnRound = new JRadioButton("Circulo");
	
	private static final Color corBranco = new Color(255, 255, 255);


	/**
	 * Create the application.
	 */
	public PaintWindow() {
		//SimplePaint sp = new SimplePaint();
		
		this.initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setVisible(true);
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 764, 75);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		final PadDraw drawPad = new PadDraw();
		drawPad.setBounds(10, 85, 764, 444);
		frame.getContentPane().add(drawPad);		
		
		JButton btnClear = new JButton(new ImageIcon(getClass().getResource("/imagens/clear.png")));
		btnClear.setBackground(corBranco);
		btnClear.setBorder(null);
		btnClear.setBounds(665, 11, 53, 53);
		panel.add(btnClear);
		
		final JCheckBox redBox = new JCheckBox("");
		redBox.setBackground(Color.RED);
		redBox.setBounds(6, 11, 21, 23);
		panel.add(redBox);
		
		final JCheckBox blackBox = new JCheckBox("");
		blackBox.setBackground(Color.BLACK);
		blackBox.setBounds(6, 37, 21, 23);
		panel.add(blackBox);
		
		final JCheckBox magentaBox = new JCheckBox("");
		magentaBox.setBackground(Color.MAGENTA);
		magentaBox.setBounds(29, 37, 21, 23);
		panel.add(magentaBox);
		
		final JCheckBox blueBox = new JCheckBox("");
		blueBox.setBackground(Color.BLUE);
		blueBox.setBounds(29, 11, 21, 23);
		panel.add(blueBox);
		
		final JCheckBox greenBox = new JCheckBox("");
		greenBox.setBackground(Color.GREEN);
		greenBox.setBounds(52, 11, 21, 23);
		panel.add(greenBox);
		
		final JCheckBox cyanBox = new JCheckBox("");
		cyanBox.setBackground(Color.CYAN);
		cyanBox.setBounds(52, 37, 21, 23);
		panel.add(cyanBox);
		
		final JCheckBox yellowBox = new JCheckBox("");
		yellowBox.setBackground(Color.YELLOW);
		yellowBox.setBounds(75, 11, 21, 23);
		panel.add(yellowBox);
		
		final JCheckBox whiteBox = new JCheckBox("");
		whiteBox.setBackground(Color.WHITE);
		whiteBox.setBounds(75, 37, 21, 23);
		panel.add(whiteBox);
		
		redBox.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				if(redBox.isSelected()){
					whiteBox.setSelected(false);
					//redBox.setSelected(false);
					blueBox.setSelected(false);
					yellowBox.setSelected(false);
					greenBox.setSelected(false);
					cyanBox.setSelected(false);
					magentaBox.setSelected(false);
					blackBox.setSelected(false);
					drawPad.red();
				}
			}
		});
		
		blueBox.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				if(blueBox.isSelected()){
					whiteBox.setSelected(false);
					redBox.setSelected(false);
					//blueBox.setSelected(false);
					yellowBox.setSelected(false);
					greenBox.setSelected(false);
					cyanBox.setSelected(false);
					magentaBox.setSelected(false);
					blackBox.setSelected(false);
					drawPad.blue();
				}
			}
		});
		
		greenBox.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				if(greenBox.isSelected()){
					whiteBox.setSelected(false);
					redBox.setSelected(false);
					blueBox.setSelected(false);
					yellowBox.setSelected(false);
					//greenBox.setSelected(false);
					cyanBox.setSelected(false);
					magentaBox.setSelected(false);
					blackBox.setSelected(false);
					drawPad.green();
				}
			}
		});
		
		yellowBox.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				if(yellowBox.isSelected()){
					whiteBox.setSelected(false);
					redBox.setSelected(false);
					blueBox.setSelected(false);
					//yellowBox.setSelected(false);
					greenBox.setSelected(false);
					cyanBox.setSelected(false);
					magentaBox.setSelected(false);
					blackBox.setSelected(false);
					drawPad.yellow();
				}
			}
		});
		
		magentaBox.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(magentaBox.isSelected()){
					whiteBox.setSelected(false);
					redBox.setSelected(false);
					blueBox.setSelected(false);
					yellowBox.setSelected(false);
					greenBox.setSelected(false);
					cyanBox.setSelected(false);
					//magentaBox.setSelected(false);
					blackBox.setSelected(false);
					drawPad.magenta();
				}
			}
		});

		cyanBox.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				if(cyanBox.isSelected()){
					whiteBox.setSelected(false);
					redBox.setSelected(false);
					blueBox.setSelected(false);
					yellowBox.setSelected(false);
					greenBox.setSelected(false);
					//cyanBox.setSelected(false);
					magentaBox.setSelected(false);
					blackBox.setSelected(false);
					drawPad.cyan();
				}
			}
		});
		
		ButtonGroup grupo = new ButtonGroup();
		
		
		rdbtnTriangulo.setBounds(246, 11, 109, 23);
		panel.add(rdbtnTriangulo);
		rdbtnTriangulo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange() == 1){
					drawPad.setShape(2);
				}
			}
		});
		
		rdbtnRetangulo.setBounds(246, 41, 109, 23);
		panel.add(rdbtnRetangulo);
		rdbtnRetangulo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange() == 1){
					drawPad.setShape(1);
				}
			}
		});

		rdbtnLinha.setSelected(true);
		rdbtnLinha.setBounds(357, 41, 109, 23);
		panel.add(rdbtnLinha);
		rdbtnLinha.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange() == 1){
					drawPad.setShape(0);
				}
			}
		});
		
		rdbtnRound.setBounds(357, 11, 109, 23);
		panel.add(rdbtnRound);
		rdbtnRound.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange() == 1){
					drawPad.setShape(3);
				}
			}
		});
		
		grupo.add(rdbtnRound);
		grupo.add(rdbtnLinha);
		grupo.add(rdbtnRetangulo);
		grupo.add(rdbtnTriangulo);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Arquivo");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmSalvar = new JMenuItem("Salvar");
		mntmSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				drawPad.salvar();

			}
		});

		mnNewMenu.add(mntmSalvar);
		
		
		whiteBox.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				if(whiteBox.isSelected()){
					//whiteBox.setSelected(false);
					redBox.setSelected(false);
					blueBox.setSelected(false);
					yellowBox.setSelected(false);
					greenBox.setSelected(false);
					cyanBox.setSelected(false);
					magentaBox.setSelected(false);
					blackBox.setSelected(false);
					drawPad.white();
				}
			}
		});
		
		blackBox.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				if(blackBox.isSelected()){
					whiteBox.setSelected(false);
					redBox.setSelected(false);
					blueBox.setSelected(false);
					yellowBox.setSelected(false);
					greenBox.setSelected(false);
					cyanBox.setSelected(false);
					magentaBox.setSelected(false);
					drawPad.black();
				}
			}
		});
		
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				drawPad.clear();
			}
		});
	}
}
