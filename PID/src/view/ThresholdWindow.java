package view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import controller.Similaridade;
import model.Mensagem;

public class ThresholdWindow{
	
	JFrame frame;
	JTextField limiar;
	
	final Color corBranco = new Color(255, 255, 255);

	Mensagem msg = new Mensagem();
	
	public ThresholdWindow() {
		initialize();
	}
	
	/**
	 * Inicializa todos os componentes do frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 350, 100);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.getContentPane().setBackground(corBranco);
		
		JLabel label = new JLabel("Escolha o limiar:");
		label.setBounds(20, 10, 300, 20);
		
		limiar = new JTextField("");
		limiar.setBounds(20, 40, 200, 20);
		limiar.setBackground(corBranco);
		
		JButton button = new JButton("OK");
		button.setBounds(270, 35, 60, 30);
		button.setFocusable(false);

		frame.add(label);
		frame.add(limiar);
		frame.add(button);
		
		/*KeyAdapter keyAdapter = new KeyAdapter() {
            public void keyPressed(KeyEvent evt) {
                if (evt.getKeyCode() == evt.VK_ENTER) {                    
                    button.doClick();
                }
            }
        };*/
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//TODO chamar metodo que realiza o threshold
					JOptionPane.showMessageDialog(null, "LIMIAR "+limiar.getText());
					frame.dispose();
			}
		});
		
		limiar.requestFocus();
		frame.setVisible(true);
	}
}
