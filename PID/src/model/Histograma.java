package model;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.jfree.data.category.DefaultCategoryDataset;

public class Histograma {
	private final static int RED_INDEX = 0;
	private final static int GREEN_INDEX = 1;
	private final static int BLUE_INDEX = 2;
	static float[] hsbValue = null;

		
	protected static DefaultCategoryDataset getRGBDataset(BufferedImage imageReader){
		
		DefaultCategoryDataset chartDataset = new DefaultCategoryDataset();
		List<TreeMap<Integer, Integer>> imageRGBFrequency = getRGBValues(imageReader);
		
		for (int i = 0; i < imageRGBFrequency.size(); i++) {
			for(Entry<Integer, Integer> entry : imageRGBFrequency.get(i).entrySet()){
				switch(i){
				case RED_INDEX:
					chartDataset.addValue(entry.getValue(), "Red", entry.getKey());
				break;
				case GREEN_INDEX:
					chartDataset.addValue(entry.getValue(), "Green", entry.getKey());
				break;
				case BLUE_INDEX:
					chartDataset.addValue(entry.getValue(), "Blue", entry.getKey());
				break;
				}
			}
		}
		
		return chartDataset;
		
	}
	
	
	
	/**
	 * Le a imagem e pega todos os valores RGB de cada pixel. 
	 * @param imageReader
	 * @return List com as informa��es RGB
	 */
	protected static List<TreeMap<Integer, Integer>> getRGBValues(BufferedImage imageReader){
		
		
		TreeMap<Integer, Integer> red = new TreeMap<Integer, Integer>();
		TreeMap<Integer, Integer> green = new TreeMap<Integer, Integer>();
		TreeMap<Integer, Integer> blue = new TreeMap<Integer, Integer>();
		ArrayList<TreeMap<Integer, Integer>> imageRGBFrequency = new ArrayList<TreeMap<Integer, Integer>>(3);
		
		for(int y = 0; y < imageReader.getHeight(); y++){
			for(int x = 0; x < imageReader.getWidth(); x++){
				
				Color color = new Color(imageReader.getRGB(x, y));
				
				if(red.containsKey(color.getRed())){
					red.replace(color.getRed(), red.get(color.getRed())+1);
				}else {
					red.put(color.getRed(), 1);
				}
				
				if(green.containsKey(color.getGreen())){
					green.replace(color.getGreen(), green.get(color.getGreen())+1);
				}else {
					green.put(color.getGreen(), 1);
				}

				if(blue.containsKey(color.getBlue())){
					blue.replace(color.getBlue(), blue.get(color.getBlue())+1);
				}else {
					blue.put(color.getBlue(), 1);
				}
				
			}
		}
		
		imageRGBFrequency.add(red);
		imageRGBFrequency.add(green);
		imageRGBFrequency.add(blue);
		
		return imageRGBFrequency;
		
	}
	
	
	/**
	 * Le a imagem e pega todos os valores YUV de cada pixel apos conversao do RGB. 
	 * @param imageReader
	 * @return List com as informacoes YUV
	 */
	protected static List<TreeMap<Integer, Integer>> getYUVValues(BufferedImage imageReader){
		

		TreeMap<Integer, Integer> red = new TreeMap<Integer, Integer>();
		TreeMap<Integer, Integer> green = new TreeMap<Integer, Integer>();
		TreeMap<Integer, Integer> blue = new TreeMap<Integer, Integer>();
		ArrayList<TreeMap<Integer, Integer>> imageRGBFrequency = new ArrayList<TreeMap<Integer, Integer>>(3);
		
		for(int y = 0; y < imageReader.getHeight(); y++){
			for(int x = 0; x < imageReader.getWidth(); x++){
				
				Color color = new Color(imageReader.getRGB(x, y));
				int redColor = color.getRed(), greenColor = color.getGreen(), blueColor = color.getBlue();
				int Yuv = 0, yUv = 0, yuV = 0;
				
				Yuv = (int)((0.299*redColor)+(0.587*greenColor)+(0.114*blueColor));
				yUv = (int)(0.492*Math.abs((blueColor - Yuv)));
				yuV = (int)(0.877*Math.abs((redColor - Yuv)));
				
			
				if(red.containsKey(Yuv)){
					red.replace(Yuv, red.get(Yuv)+1);
					//System.out.println("Entrou");
				}else {
					red.put(Yuv, 1);
				}
				
				if(green.containsKey(yUv)){
					//System.out.println("Entrou");
					green.replace(yUv, green.get(yUv)+1);
				}else {
					green.put(yUv, 1);
				}

				if(blue.containsKey(yuV)){
					//System.out.println("Entrou");
					blue.replace(yuV, blue.get(yuV)+1);
				}else {
					blue.put(yuV, 1);
				}
			}
				
		}

		imageRGBFrequency.add(red);
		imageRGBFrequency.add(green);
		imageRGBFrequency.add(blue);
		
		return imageRGBFrequency;
		
	}


/**
 * Le a imagem e pega todos os valores HSV de cada pixel ap�s convers�o do RGB. 
 * @param imageReader
 * @return List com as informa��es HSV
 */
protected static List<TreeMap<Integer, Integer>> getHSVValues(BufferedImage imageReader){
	
	TreeMap<Integer, Integer> red = new TreeMap<Integer, Integer>();
	TreeMap<Integer, Integer> green = new TreeMap<Integer, Integer>();
	TreeMap<Integer, Integer> blue = new TreeMap<Integer, Integer>();
	ArrayList<TreeMap<Integer, Integer>> imageRGBFrequency = new ArrayList<TreeMap<Integer, Integer>>(3);
	
	for(int y = 0; y < imageReader.getHeight(); y++){
		for(int x = 0; x < imageReader.getWidth(); x++){
			
			Color color = new Color(imageReader.getRGB(x, y));
			int redColor = color.getRed(), greenColor = color.getGreen(), blueColor = color.getBlue();
			int hue = 0, saturation = 0, brightness = 0;
			float redNormalizado = redColor/255;
			float greenNormalizado = greenColor/255;
			float blueNormalizado = blueColor/255;
			
			float Cmax = Math.max(Math.max(redNormalizado, greenNormalizado), blueNormalizado);
			float Cmin = Math.min(Math.min(redNormalizado, greenNormalizado), blueNormalizado);
			float delta = Cmax - Cmin;
			
			if(Cmax == redNormalizado && greenNormalizado >= blueNormalizado){
				hue = (int)(60 * ((greenNormalizado - blueNormalizado)/(Cmax - Cmin)) + 0);
			}else if(Cmax == redNormalizado && greenNormalizado < blueNormalizado){
				hue = (int)(60 * ((greenNormalizado - blueNormalizado)/(Cmax - Cmin)) + 360);
			}else if(Cmax == greenNormalizado){
				hue = (int)(60 * ((blueNormalizado - redNormalizado)/(Cmax - Cmin)) + 120);
			}else if(Cmax == blueNormalizado){
				hue = (int)(60 * ((redNormalizado - greenNormalizado)/(Cmax - Cmin)) + 240);
			}
			
			saturation = (int)(Cmax == 0 ? 0 : delta/Cmax);
			brightness = (int)Cmax;
			
			if(red.containsKey(hue)){
				red.replace(hue, red.get(hue)+1);
				//System.out.println("Entrou");
			}else {
				red.put(hue, 1);
			}
			
			if(green.containsKey(saturation)){
				//System.out.println("Entrou");
				green.replace(saturation, green.get(saturation)+1);
			}else {
				green.put(saturation, 1);
			}

			if(blue.containsKey(brightness)){
				//System.out.println("Entrou");
				blue.replace(brightness, blue.get(brightness)+1);
			}else {
				blue.put(brightness, 1);
			}
		}
			
		}

	imageRGBFrequency.add(red);
	imageRGBFrequency.add(green);
	imageRGBFrequency.add(blue);
	
	return imageRGBFrequency;
	
}

protected static List<TreeMap<Integer, Integer>> getRGBNValues(BufferedImage imageReader){
	
	
	TreeMap<Integer, Integer> red = new TreeMap<Integer, Integer>();
	TreeMap<Integer, Integer> green = new TreeMap<Integer, Integer>();
	TreeMap<Integer, Integer> blue = new TreeMap<Integer, Integer>();
	ArrayList<TreeMap<Integer, Integer>> imageRGBFrequency = new ArrayList<TreeMap<Integer, Integer>>(3);
	
	for(int y = 0; y < imageReader.getHeight(); y++){
		for(int x = 0; x < imageReader.getWidth(); x++){
			
			Color color = new Color(imageReader.getRGB(x, y));
			int redNormalizado = color.getRed()/255;
			int greenNormalizado = color.getGreen()/255;
			int blueNormalizado = color.getBlue()/255;
			
			if(red.containsKey(redNormalizado)){
				red.replace(redNormalizado, red.get(redNormalizado)+1);
			}else {
				red.put(redNormalizado, 1);
			}
			
			if(green.containsKey(greenNormalizado)){
				green.replace(greenNormalizado, green.get(greenNormalizado)+1);
			}else {
				green.put(greenNormalizado, 1);
			}

			if(blue.containsKey(blueNormalizado)){
				blue.replace(blueNormalizado, blue.get(blueNormalizado)+1);
			}else {
				blue.put(blueNormalizado, 1);
			}
			
		}
	}
	
	imageRGBFrequency.add(red);
	imageRGBFrequency.add(green);
	imageRGBFrequency.add(blue);
	
	return imageRGBFrequency;
	
}


/**
 * Le a imagem e pega todos os valores YUV de cada pixel ap�s convers�o do RGB. 
 * @param imageReader
 * @return List com as informa��es YUV
 */
protected static List<TreeMap<Integer, Integer>> getYUVNValues(BufferedImage imageReader){
	

	TreeMap<Integer, Integer> red = new TreeMap<Integer, Integer>();
	TreeMap<Integer, Integer> green = new TreeMap<Integer, Integer>();
	TreeMap<Integer, Integer> blue = new TreeMap<Integer, Integer>();
	ArrayList<TreeMap<Integer, Integer>> imageRGBFrequency = new ArrayList<TreeMap<Integer, Integer>>(3);
	
	for(int y = 0; y < imageReader.getHeight(); y++){
		for(int x = 0; x < imageReader.getWidth(); x++){
			
			Color color = new Color(imageReader.getRGB(x, y));
			int redColor = color.getRed()/255, greenColor = color.getGreen()/255, blueColor = color.getBlue()/255;
			int Yuv = 0, yUv = 0, yuV = 0;
			
			Yuv = (int)((0.299*redColor)+(0.587*greenColor)+(0.114*blueColor));
			yUv = (int)(0.492*Math.abs((blueColor - Yuv)));
			yuV = (int)(0.877*Math.abs((redColor - Yuv)));
			
		
			if(red.containsKey(Yuv)){
				red.replace(Yuv, red.get(Yuv)+1);
				//System.out.println("Entrou");
			}else {
				red.put(Yuv, 1);
			}
			
			if(green.containsKey(yUv)){
				//System.out.println("Entrou");
				green.replace(yUv, green.get(yUv)+1);
			}else {
				green.put(yUv, 1);
			}

			if(blue.containsKey(yuV)){
				//System.out.println("Entrou");
				blue.replace(yuV, blue.get(yuV)+1);
			}else {
				blue.put(yuV, 1);
			}
		}
			
	}

	imageRGBFrequency.add(red);
	imageRGBFrequency.add(green);
	imageRGBFrequency.add(blue);
	
	return imageRGBFrequency;
	
}


/**
 * Le a imagem e pega todos os valores HSV de cada pixel ap�s convers�o do RGB. 
 * @param imageReader
 * @return List com as informa��es HSV
 */
protected static List<TreeMap<Integer, Integer>> getHSVNValues(BufferedImage imageReader){
	
	TreeMap<Integer, Integer> red = new TreeMap<Integer, Integer>();
	TreeMap<Integer, Integer> green = new TreeMap<Integer, Integer>();
	TreeMap<Integer, Integer> blue = new TreeMap<Integer, Integer>();
	ArrayList<TreeMap<Integer, Integer>> imageRGBFrequency = new ArrayList<TreeMap<Integer, Integer>>(3);
	
	for(int y = 0; y < imageReader.getHeight(); y++){
		for(int x = 0; x < imageReader.getWidth(); x++){
			
			Color color = new Color(imageReader.getRGB(x, y));
			int redColor = color.getRed()/255, greenColor = color.getGreen()/255, blueColor = color.getBlue()/255;
			int hue = 0, saturation = 0, brightness = 0;
			float redNormalizado = redColor/255;
			float greenNormalizado = greenColor/255;
			float blueNormalizado = blueColor/255;
			
			float Cmax = Math.max(Math.max(redNormalizado, greenNormalizado), blueNormalizado);
			float Cmin = Math.min(Math.min(redNormalizado, greenNormalizado), blueNormalizado);
			float delta = Cmax - Cmin;
			
			if(Cmax == redNormalizado && greenNormalizado >= blueNormalizado){
				hue = (int)(60 * ((greenNormalizado - blueNormalizado)/(Cmax - Cmin)) + 0);
			}else if(Cmax == redNormalizado && greenNormalizado < blueNormalizado){
				hue = (int)(60 * ((greenNormalizado - blueNormalizado)/(Cmax - Cmin)) + 360);
			}else if(Cmax == greenNormalizado){
				hue = (int)(60 * ((blueNormalizado - redNormalizado)/(Cmax - Cmin)) + 120);
			}else if(Cmax == blueNormalizado){
				hue = (int)(60 * ((redNormalizado - greenNormalizado)/(Cmax - Cmin)) + 240);
			}
			
			saturation = (int)(Cmax == 0 ? 0 : delta/Cmax);
			brightness = (int)Cmax;
			
			if(red.containsKey(hue)){
				red.replace(hue, red.get(hue)+1);
				//System.out.println("Entrou");
			}else {
				red.put(hue, 1);
			}
			
			if(green.containsKey(saturation)){
				//System.out.println("Entrou");
				green.replace(saturation, green.get(saturation)+1);
			}else {
				green.put(saturation, 1);
			}

			if(blue.containsKey(brightness)){
				//System.out.println("Entrou");
				blue.replace(brightness, blue.get(brightness)+1);
			}else {
				blue.put(brightness, 1);
			}
		}
			
		}

	imageRGBFrequency.add(red);
	imageRGBFrequency.add(green);
	imageRGBFrequency.add(blue);
	
	return imageRGBFrequency;
	
}
}
