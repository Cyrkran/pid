package model;

public class Fourier {
	int [][] imageData = null;
	double [][] real = null;
	double [][] imaginario = null;
	double [][] amplitude = null;
	int width = 0;
	int height = 0;
	
	public Fourier(){
		imageData = null;
		real = null;
		imaginario = null;
		amplitude = null;
		width = 0;
		height = 0;
	}
	
	public Fourier(int height, int width){
		this.width = width;
		this.height = height;
		imageData = new int[height][width];
		real = new double[height][width];
		imaginario = new double[height][width];
		amplitude = new double[height][width];
	}
	
	public Fourier(int [][]imageData, int width, int height){
		this.imageData = imageData;
		this.width = width;
		this.height = height;
		real = new double[height][width];
		imaginario = new double[height][width];
		amplitude = new double[height][width];
	}
	
	public int[][] getImageData() {
		return imageData;
	}
	
	
	public void setImageData(int[][] imageData) {
		this.imageData = imageData;
	}
	
	public void setImageData(int i, int j, int valor) {
		this.imageData[i][j] = valor;
	}
	
	
	public double[][] getReal() {
		return real;
	}
	
	public double getReal(int i, int j) {
		return real[i][j];
	}
	
	
	public void setReal(int i, int j, double valor) {
		this.real[i][j] += valor;
	}
	
	
	public double getImaginario(int i, int j) {
		return imaginario[i][j];
	}
	
	
	public void setImaginario(double[][] imaginario) {
		this.imaginario = imaginario;
	}
	
	public void setImaginario(int i, int j, double valor) {
		this.imaginario[i][j] -= valor;
	}
	
	public void setSomaImaginario(int i, int j, double valor) {
		this.imaginario[i][j] += valor;
	}
	
	
	public double[][] getAmplitude() {
		return amplitude;
	}
	
	
	public void setAmplitude(int i, int j, double valor) {
		this.amplitude[i][j] = valor;
	}
	
	
	public int getWidth() {
		return width;
	}

	
	public void setWidth(int width) {
		this.width = width;
	}

	
	public int getHeight() {
		return height;
	}

	
	public void setHeight(int height) {
		this.height = height;
	}

}
