package model;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import javax.imageio.ImageIO;
import javax.swing.*;


public class PadDraw extends JComponent{
	Image image;
	//this is gonna be your image that you draw on
	Graphics2D graphics2D;
	//this is what we'll be using to draw on
	int currentX, currentY, oldX, oldY;
	//these are gonna hold our mouse coordinates
	int shape = 0;
	
	public void setShape(int shape){
		this.shape = shape;
	}
	
	//Now for the constructors
	public PadDraw(){
		setDoubleBuffered(false);
		addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent e){
				oldX = e.getX();
				oldY = e.getY();
			}
		});
		//if the mouse is pressed it sets the oldX & oldY
		//coordinates as the mouses x & y coordinates
		addMouseMotionListener(new MouseMotionAdapter(){
			public void mouseDragged(MouseEvent e){
				
				/**
				 * caso 0: Linha
				 * caso 1: Retangulo
				 * caso 2: Triangulo
				 * caso 3: Circulo
				 */
				if (shape == 0) {
					currentX = e.getX();
					currentY = e.getY();
					if(graphics2D != null)
						graphics2D.drawLine(oldX, oldY, currentX, currentY);
					repaint();
					oldX = currentX;
					oldY = currentY;
				}
				
			}
		});
		//while the mouse is dragged it sets currentX & currentY as the mouses x and y
		//then it draws a line at the coordinates
		//it repaints it and sets oldX and oldY as currentX and currentY
		
		addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

				switch (shape) {
				case 1:
					currentX = e.getX();
					currentY = e.getY();
					if(graphics2D != null)
						graphics2D.drawRect(oldX, oldY, currentX-oldX, currentY-oldY);
					repaint();
					oldX = currentX;
					oldY = currentY;
					break;

				case 2:
					
					currentX = e.getX();
					currentY = e.getY();
					if(graphics2D != null)
						graphics2D.draw(new RoundRectangle2D.Double(oldX, oldY,
                                currentX - oldX,
                                currentY - oldY,
                                10, 10));
					
					repaint();
					oldX = currentX;
					oldY = currentY;
					
					break;
					
				case 3:
					currentX = e.getX();
					currentY = e.getY();
					if(graphics2D != null)
						graphics2D.draw(new Ellipse2D.Double(oldX, oldY,
                            currentX-oldX,
                            currentY-oldY));
					repaint();
					oldX = currentX;
					oldY = currentY;
					break;
					
				default:
					break;
				}

			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	public void paintComponent(Graphics g){
		if(image == null){
			image = createImage(getSize().width, getSize().height);
			graphics2D = (Graphics2D)image.getGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			clear();

		}
		g.drawImage(image, 0, 0, null);
		
		//ImageIO.write(image, ".jpg", new FileOutputStream("C:\\"));
	}
	//this is the painting bit
	//if it has nothing on it then
	//it creates an image the size of the window
	//sets the value of Graphics as the image
	//sets the rendering
	//runs the clear() method
	//then it draws the image


	public void clear(){
		graphics2D.setPaint(Color.white);
		graphics2D.fillRect(0, 0, getSize().width, getSize().height);
		graphics2D.setPaint(Color.black);
		repaint();
	}
	//this is the clear
	//it sets the colors as white
	//then it fills the window with white
	//thin it sets the color back to black
	public void red(){
		graphics2D.setPaint(Color.red);
		repaint();
	}
	//this is the red paint
	public void black(){
		graphics2D.setPaint(Color.black);
		repaint();
	}
	//black paint
	public void magenta(){
		graphics2D.setPaint(Color.magenta);
		repaint();
	}
	//magenta paint
	public void blue(){
		graphics2D.setPaint(Color.blue);
		repaint();
	}
	//blue paint
	public void green(){
		graphics2D.setPaint(Color.green);
		repaint();
	}
	//green paint
	
	public void white(){
		graphics2D.setPaint(Color.white);
		repaint();
	}
	
	public void cyan(){
		graphics2D.setPaint(Color.cyan);
		repaint();
	}
	
	public void yellow(){
		graphics2D.setPaint(Color.yellow);
		repaint();
	}
	
	public void customColor(Color c){
		graphics2D.setPaint(c);
		repaint();		
	}

	public void salvar() {
				
		BufferedImage image = new BufferedImage(764,444, BufferedImage.TYPE_INT_RGB);
        JFileChooser jFile = new JFileChooser();
        jFile.showSaveDialog(null);
        
        if(jFile.getSelectedFile() != null){
        	Path pth = jFile.getSelectedFile().toPath();
            JOptionPane.showMessageDialog(null, "Imagem salva com sucesso!");
            try {

                Graphics2D g = image.createGraphics();
               // g.drawImage(image, 0,  0,null);
                paint(g);
                ImageIO.write(image, "png", new File(pth.toString() + ".png"));

            } catch (IOException ox) {

            }
        }
        
		
	}
}