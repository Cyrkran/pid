package model;

public class Quicksort {

	double[][] result = new double[0][0];
	
	
	public Quicksort(double[][] result){
		this.result = result;
	}
	
	
	/**
	 * Algoritmo de ordenacao Quicksort.
	 * @param esq - Inteiro que indica o inicio do array a ser ordenado.
	 * @param dir - Inteiro que indica o fim do array a ser ordenado.
	 */
    public void quicksort(int esq, int dir) {
        int i = esq, j = dir;
        double pivo = result[1][(dir+esq)/2];
        while (i <= j) {
            while (result[1][i] < pivo) i++;
            while (result[1][j] > pivo) j--;
            if (i <= j) {
            	swap(i, j);

                i++;
                j--;
            }
        }
        if (esq < j)  quicksort(esq, j);
        if (i < dir)  quicksort(i, dir);
    }
	
    
    /**
     * Troca o conteudo de duas colunas da matriz.
     * @param i - Inteiro que indica a primeira posicao.
     * @param j - Inteiro que indica a segunda posicao.
     */
    public void swap(int i, int j) {
       double temp = result[0][i];
       result[0][i] = result[0][j];
       result[0][j] = temp;
       
       temp = result[1][i];
       result[1][i] = result[1][j];
       result[1][j] = temp;
       
    }
    

	public double[][] getResult() {
		return result;
	}

	
	public void setResult(double[][] result) {
		this.result = result;
	}
}
