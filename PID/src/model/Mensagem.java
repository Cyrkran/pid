package model;

import javax.swing.JOptionPane;

/**
 * Classe para centralizacao dos possiveis erros que aparecerao no programa e das mensagens que deverao
 * ser retornadas ao usuario.
 */
public class Mensagem {
	
	/**
	 * Define as possiveis mensagens de erro do sistema. 
	 * @param e - Codigo de erro.
	 */
	public void error(int e){
		switch(e){
		case 0:
			JOptionPane.showMessageDialog(null, "Erro ao executar classe principal.");
		break;
		case 1:
			JOptionPane.showMessageDialog(null, "Erro ao abrir imagem.");
		break;
		case 2:
			JOptionPane.showMessageDialog(null, "Erro ao redimensionar imagem.");
		break;
		case 3:
			JOptionPane.showMessageDialog(null, "Erro ao preencher a lista.");
		break;
		case 4:
			JOptionPane.showMessageDialog(null, "Erro ao escolher imagem.");
		break;
		case 5:
			JOptionPane.showMessageDialog(null, "Erro ao gerar histograma.");
		break;
		case 6:
			JOptionPane.showMessageDialog(null, "Erro ao ordenar imagens.");
		break;
		case 7:
			JOptionPane.showMessageDialog(null, "Erro ao abrir javadoc.");
		break;
		case 8:
			JOptionPane.showMessageDialog(null, "Erro ao gerar imagem filtrada.");
		break;
		case 9:
			JOptionPane.showMessageDialog(null, "Erro ao aplicar o filtro na imagem.");
		break;
		default:
			JOptionPane.showMessageDialog(null, "Erro!");

		}
		
	}
	
	
	/**
	 * Mensagem que exibe os nomes dos desenvolvedores.
	 */
	public void desenvolvedores(){
		JOptionPane.showMessageDialog(null, "Kelly Hanna Malaquias - kelly.malaquias@sga.pucminas.br - 508403 "
									+ "\nMiguel Luiz Magalhães de Sousa - miguel.sousa@sga.pucminas.br - 463985 "
									+ "\nPaulo Souza Schaper - paulo.schaper@sga.pucminas.br - 466557 ",
									"Desenvolvedores", JOptionPane.INFORMATION_MESSAGE);
		
		
	}
	
	
	/**
	 * Define as possiveis mensagens de alerta do sistema. 
	 * @param m - Codigo de mensagem.
	 */
	public void mensagem(int m){
		switch(m){
		case 0:
			JOptionPane.showMessageDialog(null, "Selecione uma imagem para obter o histograma.");
			break;
		case 1:
			JOptionPane.showMessageDialog(null, "Clicou no botao ok");
			break;
		case 2:
			JOptionPane.showMessageDialog(null, "ATENÇÃO: Você terá que aguardar alguns minutos enquanto os histogramas são gerados e ordenados!\n"
					+ " POR FAVOR não desista! Obrigada :D. TEMPO ESTIMADO: +- 3 min.");
			break;
		case 3:
			JOptionPane.showMessageDialog(null, "É o seu primeiro acesso, aguarde enquanto os histogramas são gerados.");

		break;
		
		default:
			JOptionPane.showMessageDialog(null, "Mensagem!");
		}
		
	}
	
	
	
	/**
	 * Define as possiveis entradas de dados do sistema. 
	 * @param e - Codigo de entrada.
	 */
	public String entrada(int e){
		String entrada = "";
		switch(e){
			case 0:
				entrada = JOptionPane.showInputDialog("Indique o limiar:");
				break;
			case 1:
				entrada = JOptionPane.showInputDialog("");
				break;
			default:
				entrada = JOptionPane.showInputDialog("Entre com os dados.");
		}
		return entrada;
	}
}
