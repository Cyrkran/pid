package controller;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import model.Mensagem;
import model.Quicksort;

public class Ordenacao {
	
	private final String PATH = "/imagens/base_de_imagens/";
	private static final String EXT = ".jpg";
		
	Mensagem msg = new Mensagem();
	
	GerarHistograma histograma = new GerarHistograma();
	
	/**
	 * Ordena as imagens de acordo com o parametro de distancia especificado 
	 * @param bi Imagem original escolhida no inicio da execucao
	 * @param medidaDistancia Medida de distancia escolhida
	 * @param opcao opcao de distancia
	 * @return array de double ordenado
	 */
	public double [][] ordenarImagens(BufferedImage bi, char medidaDistancia, String opcao){
		double [][] result = new double[2][1000];
		switch(medidaDistancia){
			case 'c': 
				result = ordenarImagensCosseno(bi, opcao);
				break;
			case 'e':
				result = ordenarImagensEuclidiana(bi, opcao);
				break;		
			case 'm':
				result = ordenarImagensManhattan(bi, opcao);
				break;		
			case 'x':
				result = ordenarImagensXadrez(bi, opcao);
				break;
			default:
				ordenarImagensEuclidiana(bi, opcao);		
				break;
		}
		
		//chamar ordenacao
		Quicksort quick = new Quicksort(result);
		quick.quicksort(0, result[1].length-1);
		
		result = quick.getResult();
		
	//	System.out.println("ORDENACAO - Fez o Quick");
		
		return result;		
	}
	
	
    /**
     * Metodo que ordena as imagens da base, usando a distancia cosseno entre os histogramas.
     * @return result - Matriz de double contendo as imagens e seus histogramas em ordem crescente de distancia. 
     */
	public double [][] ordenarImagensCosseno(BufferedImage bi, String opcao){
		double [][] result = new double[2][1000];
		BufferedImage bi2 = null;
		
		for(int i = 0; i < 1000; i++){
			try {
				bi2 = ImageIO.read(getClass().getResource(PATH + String.valueOf(i) + EXT));
				result[0][i] = (double) i;
				result[1][i] = histograma.distancia(bi, bi2, opcao, 'c');
			} catch (IOException e) {
				msg.error(6);
			}
		//	System.out.println("ORDENACAO - Imagem: "+i);

		}
		
		return result;
	}
	
	
    /**
     * Metodo que ordena as imagens da base, usando a distancia euclidiana entre os histogramas.
     * @return result - Matriz de double contendo as imagens e seus histogramas em ordem crescente de distancia. 
     */
	public double [][] ordenarImagensEuclidiana(BufferedImage bi, String opcao){
		double [][] result = new double[2][1000];
		BufferedImage bi2 = null;
		
		for(int i = 0; i < 1000; i++){
			try {
				bi2 = ImageIO.read(getClass().getResource(PATH + String.valueOf(i) + EXT));
				result[0][i] = (double) i;
				result[1][i] = histograma.distancia(bi, bi2, opcao, 'e');
			} catch (IOException e) {
				msg.error(6);
			}
			
		//	System.out.println("ORDENACAO - Imagem: "+i);
		}
		
		return result;
	}
	
	
    /**
     * Metodo que ordena as imagens da base, usando a distancia manhattan entre os histogramas.
     * @return result - Matriz de double contendo as imagens e seus histogramas em ordem crescente de distancia. 
     */
	public double [][] ordenarImagensManhattan(BufferedImage bi, String opcao){
		double [][] result = new double[2][1000];
		BufferedImage bi2 = null;
		
		for(int i = 0; i < 1000; i++){
			try {
				bi2 = ImageIO.read(getClass().getResource(PATH + String.valueOf(i) + EXT));
				result[0][i] = (double) i;
				result[1][i] = histograma.distancia(bi, bi2, opcao,'m');
			} catch (IOException e) {
				msg.error(6);
			}
	//		System.out.println("ORDENACAO - Imagem: "+i);
		}
		
		return result;
	}
	
	
    /**
     * Metodo que ordena as imagens da base, usando a distancia xadrez entre os histogramas.
     * @return result - Matriz de double contendo as imagens e seus histogramas em ordem crescente de distancia. 
     */
	public double [][] ordenarImagensXadrez(BufferedImage bi, String opcao){
		double [][] result = new double[2][1000];
		BufferedImage bi2 = null;
		
		for(int i = 0; i < 1000; i++){
			try {
				bi2 = ImageIO.read(getClass().getResource(PATH + String.valueOf(i) + EXT));
				result[0][i] = (double) i;
				result[1][i] = histograma.distancia(bi, bi2, opcao, 'x');
			} catch (IOException e) {
				msg.error(6);
			}
	//		System.out.println("ORDENACAO - Imagem: "+i);
		}
		
		return result;
	}
}
