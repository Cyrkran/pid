package controller;

import model.Fourier;
import model.Mensagem;

/**
 * Classe retirada e adaptada do site: http://www.sanfoundry.com/java-perform-2d-fft-inplace-given-complex-2d-array/
 */ 
public class FFT{
	
	Mensagem msg = new Mensagem();

	Fourier fourier = new Fourier();
	Fourier inversa = new Fourier();

	
  //  public int [][] twoDfft(int[][] inputData, double[][] realOut, double[][] imagOut, double[][] amplitudeOut) {
    public Fourier DFT(int[][] inputData) {
    	//Criar objeto fourier
    	fourier = new Fourier(inputData.length, inputData[0].length);
  
        // Two outer loops iterate on output data.
        for (int yWave = 0; yWave < fourier.getHeight(); yWave++){
            for (int xWave = 0; xWave < fourier.getWidth(); xWave++){
                // Two inner loops iterate on input data.
                for (int ySpace = 0; ySpace < fourier.getHeight(); ySpace++){
                    for (int xSpace = 0; xSpace < fourier.getWidth(); xSpace++){
                        // Calcular real, imaginario e amplitude.
                        fourier.setReal(yWave, xWave, inputData[ySpace][xSpace] * Math.cos(2
                                        * Math.PI * ((1.0 * xWave * xSpace / fourier.getWidth()) + (1.0
                                        * yWave * ySpace / fourier.getHeight())))
                                / Math.sqrt(fourier.getWidth() * fourier.getHeight()));
                        
                        fourier.setImaginario(yWave, xWave, inputData[ySpace][xSpace] * Math.sin(2
                                        * Math.PI * ((1.0 * xWave * xSpace / fourier.getWidth()) + (1.0
                                        * yWave * ySpace / fourier.getHeight())))
                                / Math.sqrt(fourier.getWidth() * fourier.getHeight()));
                        fourier.setAmplitude(yWave, xWave, Math.sqrt(fourier.getReal(yWave, xWave)
                                        * fourier.getReal(yWave, xWave)
                                        + fourier.getImaginario(yWave,xWave)
                                        * fourier.getImaginario(yWave,xWave)));
                    }
                }
                // salvar valores no imageData
                fourier.setImageData(yWave,xWave, (int) Math.sqrt(Math.pow(fourier.getReal(yWave, xWave), 2) + 
                										Math.pow(fourier.getImaginario(yWave, xWave), 2)));
            }
        }
      
        return fourier;
    }
    
    
    public Fourier inversa (int [][] imagemFourier) {
   		inversa = new Fourier(fourier.getHeight(), fourier.getWidth());
 
    	// Two outer loops iterate on output data.
    	for (int yWave = 0; yWave < fourier.getHeight(); yWave++){
    		for (int xWave = 0; xWave < fourier.getWidth(); xWave++){
    			// Two inner loops iterate on input data.
    			for (int ySpace = 0; ySpace < fourier.getHeight(); ySpace++){
    				for (int xSpace = 0; xSpace < fourier.getWidth(); xSpace++){
    					// Calcula real, imaginario e amplitude.
    					inversa.setReal(yWave, xWave, fourier.getReal(ySpace, xSpace) * Math.cos(2
    									* Math.PI * ((1.0 * xWave * xSpace / fourier.getWidth()) + (1.0
    											* yWave * ySpace / fourier.getHeight()))));
    						inversa.setSomaImaginario(yWave, xWave ,fourier.getImaginario(ySpace, xSpace) * Math.sin(2
                                       	* Math.PI * ((1.0 * xWave * xSpace / fourier.getWidth()) + (1.0
                                       			* yWave * ySpace / fourier.getHeight()))));
    						inversa.setImageData(yWave, xWave,(int) (inversa.getReal(yWave, xWave) + 
   											inversa.getImaginario(yWave, xWave)));
                   }
               }
           }
    	}
        return inversa;
    }
    
    
    public int [][] fourier(int [][] imageData){
    	Fourier espectro = DFT(imageData);
        return espectro.getImageData();
    }
    
    public int [][] fourierInversa(int [][] imageData){
    	Fourier inversa = inversa(fourier.getImageData());                
        return inversa.getImageData();
    }
    
    
    public int maior(int [][] imagem){
    	int maior = 0;
    	
    	maior = imagem[0][0];
    	for(int i = 0; i < imagem[0].length; i++){
    		for(int j = 0; j < imagem.length; j++){
    			if(imagem[i][j] > maior){
    				maior = imagem[i][j];
    			} 
    		}
    	}
    	return maior;
    }
}