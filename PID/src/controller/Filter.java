package controller;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import ij.process.ImageProcessor;

import javax.imageio.ImageIO;

import model.Mensagem;

public class Filter {
	ImageProcessor ip;
	
	public static final String PATH = System.getProperty("user.dir") + System.getProperty("file.separator") + "bin" + 
					System.getProperty("file.separator") + "imagens" +System.getProperty("file.separator") + "filtros" + 
					System.getProperty("file.separator");

	final int[][] mascaraSobelHorizontal = mascaraSobelHorizontal();

	Mensagem msg = new Mensagem();

	File imagem = null;
	
	FFT fourier = new FFT();
;

	public Filter(File file) {
		imagem = file;
	}
	
	
	private int[][] mascaraSobelHorizontal() {
		int[][] mascara = new int[3][3];
		
		mascara[0][0] = -1;
		mascara[0][1] = 0;
		mascara[0][2] = 1;
		mascara[1][0] = -2;
		mascara[1][1] = 0;
		mascara[1][2] = 2;
		mascara[2][0] = -1;
		mascara[2][1] = 0;
		mascara[2][2] = 1;
		
		return mascara;
	}


	/**
	 * Le a imagem e cria um vetor 2D dela para efetuar as operacoes.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return imageData - Array de dados da imagem.
	 */
	public int[][] getPictureFile(File file){
		int[][] imageData = null; 
		try {
			BufferedImage newImage = ImageIO.read(file);
			int maxHeight = newImage.getHeight(), maxWidth = newImage.getWidth();
			
			//criar array 2D da imagem
			imageData = new int[maxWidth][maxHeight];
			for(int i = 0; i < maxWidth; i++){
				for(int j = 0; j < maxHeight; j++){
					
					Color c = new Color(newImage.getRGB(i, j));
					imageData[i][j] = (int) ((c.getRed() * 0.3) + (c.getGreen() * 0.59) + (c.getBlue() * 0.11));
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return imageData;	
	}

	
	/**
	 * Chama o metodo para aplicacao do filtro de maximo na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return aplicarFiltro() - Metodo que aplica o filtro correspondente ao parametro passado, salva e
	 * retorna o nome da imagem. 
	 */
	public String filtroMaximo(File file){
		return aplicarFiltro(file, 1);
	}
	
	
	/**
	 * Chama o metodo para aplicacao do filtro de minimo na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return aplicarFiltro() - Metodo que aplica o filtro correspondente ao parametro passado, salva e
	 * retorna o nome da imagem. 
	 */
	public String filtroMinimo(File file){
		return aplicarFiltro(file, 2);
	}
	
	
	/**
	 * Chama o metodo para aplicacao do filtro de borda Sobel na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return aplicarFiltroSobel() - Metodo que aplica o filtro de borda Sobel ao parametro passado, salva e
	 * retorna o nome da imagem. 
	 */
	public String filtroBordaSobel(File file){		
		return aplicarFiltroSobel(file);
	}
	
	
	/**
	 * Chama o metodo para aplicacao do filtro de borda Prewitt na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return aplicarFiltroPrewitt() - Metodo que aplica o filtro de borda Prewitt ao parametro passado, salva e
	 * retorna o nome da imagem. 
	 */
	public String filtroBordaPrewitt(File file){		
		return aplicarFiltroPrewitt(file);
	}
	
	
	/**
	 * Chama o metodo para aplicacao do filtro de borda Riberts na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return aplicarFiltroRoberts() - Metodo que aplica o filtro de borda Roberts ao parametro passado, salva e
	 * retorna o nome da imagem. 
	 */
	public String filtroBordaRoberts(File file){		
		return aplicarFiltroRoberts(file);
	}
	
	
	/**
	 * Chama o metodo para aplicacao do filtro de mediana na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return aplicarFiltro() - Metodo que aplica o filtro correspondente ao parametro passado, salva e
	 * retorna o nome da imagem. 
	 */
	public String filtroMediana(File file){
		return aplicarFiltro(file, 4);
	}
	
	
	/**
	 * Chama o metodo para aplicacao do filtro de media na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return aplicarFiltro() - Metodo que aplica o filtro correspondente ao parametro passado, salva e
	 * retorna o nome da imagem. 
	 */
	public String filtroMedia(File file){
		return aplicarFiltro(file, 5);
	}
	
	
	/**
	 * Chama o metodo para aplicacao do filtro de binarizacao na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return aplicarFiltro() - Metodo que aplica o filtro correspondente ao parametro passado, salva e
	 * retorna o nome da imagem. 
	 */
	public String filtroBinarizacao(File file){
		return aplicarFiltroBinarizacao(file, 6);
	}
	
	
	/**
	 * Chama o metodo para aplicacao do filtro monocromatico na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return aplicarFiltro() - Metodo que aplica o filtro correspondente ao parametro passado, salva e
	 * retorna o nome da imagem. 
	 */
	public String filtroMonocromatico(File file){
		return 	aplicarFiltroMonocromatico(file);
	}
	
	
	/**
	 * Chama o metodo para aplicacao da transformada de Fourier na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return aplicarTransformadaFourier() - Metodo que aplica a transformada de Fourier, salva e
	 * retorna o nome da imagem após a transformacao. 
	 */
	public String transformadaFourier(File file){		
		return aplicarTransformadaFourier(file);
	}

	
	/**
	 * Chama o metodo para aplicacao da transformada de Fourier inversa na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return aplicarFiltro() - Metodo que aplica a inversa da transformada de Fourier, salva e
	 * retorna o nome da imagem. 
	 */
	public String transformadaInversa(File file){
		return aplicarInversaFourier(file);
	}
	
	
	/**
	 * Aplica a transformada de Fourier na imagem do File passado por parametro.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return gerarImagemFiltrada() - Metodo que salva a imagem, nomeia com nome+Fourier e retorna o nome da imagem salva.
	 */
	public String aplicarTransformadaFourier(File file){
		int[][] imageData = getPictureFile(file);
		int[][] newImageData;
		BufferedImage image;
		BufferedImage theImage = null;
		
		int x = 0, y = 0;
		try {
			image = ImageIO.read(file);
			int maxHeight = image.getHeight(), maxWidth = image.getWidth(); //largura e altura da imagem

			
			newImageData = fourier.fourier(imageData);
			
			int R = fourier.maior(newImageData); // pegar maior valor na matriz de dados da imagem
			
			double cn = 255.0/Math.log(1 + Math.abs(R));
			
			//Transformar o array de dados da imagem em imagem novamente
			theImage = new BufferedImage(maxWidth, maxHeight, image.getType());
				int value;
				for(int i = 0; i < maxWidth; i++){
				    for(int j = 0; j < maxHeight; j++){

				    	value = (int) Math.log(1 + newImageData[i][j]) *  (int) cn; // normalizar entre 0 e 255
				    	
				    	Color c = new Color(value, value, value);
				    	int red = (int)(c.getRed());
				    	int green = (int)(c.getGreen());
				    	int blue = (int)(c.getBlue());
				    	c = new Color(red, green, blue);
				    	// aproximar bordas para o centro
				    	x = (maxWidth/2) + (maxWidth/2 - i) - 1;
				    	y = (maxHeight/2) + (maxHeight/2 - j) - 1;
				    	theImage.setRGB(x, y, c.getRGB());
				    }
				}
			} catch (Exception e) {
				msg.mensagem(9);
			}
		
		return gerarImagemFiltrada(theImage, 8);
	}
	
	
	/**
	 * Aplica a inversa da transformada de Fourier na imagem do File passado por parametro.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return gerarImagemFiltrada() - Metodo que salva a imagem, nomeia com nome+Inversa e retorna o nome da imagem salva.
	 */
	public String aplicarInversaFourier(File file){
		int[][] imageData = getPictureFile(file);
		int[][] newImageData;
		BufferedImage image;
		BufferedImage theImage = null;
					
		try {
			image = ImageIO.read(file);
			int maxHeight = image.getHeight(), maxWidth = image.getWidth(); //largura e altura da imagem
			int [][] imagemFourier = fourier.fourier(imageData);
			
			newImageData = fourier.fourierInversa(imagemFourier);

			//Transformar o array de dados da imagem em imagem novamente
			theImage = new BufferedImage(maxWidth, maxHeight, image.getType());
			int value = 0;
						
			for(int i = 0; i < maxWidth; i++){
				for(int j = 0; j < maxHeight; j++){			    	
					value = newImageData[i][j]/255; 	//normalizar
			    				    	
					//normalizar valores superiores a 255 ou inferiores a 0
					if(value < 0){
						value = 0;
					} else if(value > 255){
						value = 255;
					}
			    
					Color c = new Color(value, value, value);
					int red = (int)(c.getRed());
					int green = (int)(c.getGreen());
					int blue = (int)(c.getBlue());
					c = new Color(red, green, blue);
					//setar valores de RGB na imagem (usando a formula para rotacao)
					theImage.setRGB(maxWidth-i-1, maxHeight-j-1, c.getRGB());
				}
			}
		} catch (IOException e) {
			msg.mensagem(9);
		}

		return gerarImagemFiltrada(theImage, 88);
	}

	
	/**
	 * Aplica o filtro monocromatico na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return gerarImagemFiltrada() - Metodo que salva a imagem, nomeia com nome+Monocromatico e retorna o nome da imagem salva.
	 */
	public String aplicarFiltroMonocromatico(File file){
		int[][] imageData = getPictureFile(file);
		BufferedImage image;
		BufferedImage theImage = null;
		try {
			image = ImageIO.read(file);
			int maxHeight = image.getHeight(), maxWidth = image.getWidth(); //largura e altura da imagem

			//Transformar o array de dados da imagem em imagem novamente
			theImage = new BufferedImage(maxWidth, maxHeight, image.getType());
			int value;
			for(int i = 0; i < maxWidth; i++){
			    for(int j = 0; j < maxHeight; j++){
			    	value = imageData[i][j];
			    	Color c = new Color(value, value, value);
			    	int red = (int)(c.getRed());
			    	int green = (int)(c.getGreen());
			    	int blue = (int)(c.getBlue());
			    	c = new Color(red, green, blue);
			    	theImage.setRGB(i, j, c.getRGB());			    	
			    }
			}
		} catch (Exception e) {
			msg.mensagem(9);
		}
		
		return gerarImagemFiltrada(theImage, 7);
	}
	
	
	/**
	 * Aplica o filtro na imagem de acordo com o valor de filtro passado por parametro.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @param filtro - Inteiro que indica o filtro a ser aplicado.
	 * @return gerarImagemFiltrada() - Metodo que salva a imagem e nomeia de acordo 
	 * com os filtros aplicados e retorna o nome da imagem salva.
	 */
	public String aplicarFiltro(File file, int filtro){
		int[][] imageData = getPictureFile(file);
		int[][] newImageData = getPictureFile(file);
		BufferedImage image;
		BufferedImage theImage = null;
		try {
			image = ImageIO.read(file);
			int maxHeight = image.getHeight(), maxWidth = image.getWidth(); //largura e altura da imagem

			int mascara[][] = mascara(1);
			
			for(int i = 1; i < maxWidth - 1; i++){
				for(int j = 1; j < maxHeight - 1; j++){
					/* le a vizinhanca de 8 e coloca todos os vizinhos em um array. 
					   Multiplica pela mascara padrao*/

					int[] array = new int[9];
					array[0] = mascara[0][0] * imageData[i-1][j-1];
					array[1] = mascara[0][1] * imageData[i-1][j];
					array[2] = mascara[0][2] * imageData[i-1][j+1];
					array[3] = mascara[1][0] * imageData[i][j-1];
					array[4] = mascara[1][1] * imageData[i][j];
					array[5] = mascara[1][2] * imageData[i+1][j+1];
					array[6] = mascara[2][0] * imageData[i+1][j-1];
					array[7] = mascara[2][1] * imageData[i+1][j];
					array[8] = mascara[2][2] * imageData[i+1][j+1];				
					
					int valor = ler3x3(array, filtro);
					newImageData[i][j] = valor;
				}
			}
			
			//Transformar o array de dados da imagem em imagem novamente
			theImage = new BufferedImage(maxWidth, maxHeight, image.getType());
				int value;
				for(int i = 0; i < maxWidth; i++){
				    for(int j = 0; j < maxHeight; j++){
				    	value = newImageData[i][j];
				    	Color c = new Color(value, value, value);
				    	int red = (int)(c.getRed());
				    	int green = (int)(c.getGreen());
				    	int blue = (int)(c.getBlue());
				    	c = new Color(red, green, blue);
				    	theImage.setRGB(i, j, c.getRGB());
				    	
				    }
				}
			} catch (Exception e) {
				msg.mensagem(9);
		}
		
		return gerarImagemFiltrada(theImage, filtro);
	}
	
	
	/**
	 * Aplica o filtro de borda Sobel na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return gerarImagemFiltrada() - Metodo que salva a imagem, nomeia com nome+Sobel
	 * e retorna o nome da imagem salva.
	 */
	public String aplicarFiltroSobel(File file){
		
		int[][] imageData = getPictureFile(file);
		int[][] newImageDataX = getPictureFile(file);
		int[][] newImageDataY = getPictureFile(file);

		BufferedImage image;
		BufferedImage theImage = null;
		try {
			image = ImageIO.read(file);
			int maxHeight = image.getHeight(), maxWidth = image.getWidth(); //largura e altura da imagem
			
			for(int i = 1; i < maxWidth - 1; i++){
				for(int j = 1; j < maxHeight - 1; j++){
					/* le a vizinhanca de 8 e coloca todos os vizinhos em um array. 
					   Multiplica pela mascara padrao*/

					int[] arrayX = new int[9];
					arrayX[0] = -1 * imageData[i-1][j-1];
					arrayX[1] =  0 * imageData[i-1][j];
					arrayX[2] =  1 * imageData[i-1][j+1];
					arrayX[3] = -2 * imageData[i][j-1];
					arrayX[4] =  0 * imageData[i][j];
					arrayX[5] =  2 * imageData[i+1][j+1];
					arrayX[6] = -1 * imageData[i+1][j-1];
					arrayX[7] =  0 * imageData[i+1][j];
					arrayX[8] =  1 * imageData[i+1][j+1];		

					
					int valorX = ler3x3(arrayX, 3);
					newImageDataX[i][j] = valorX;
					
					int[] arrayY = new int[9];
					arrayY[0] = -1 * imageData[i-1][j-1];
					arrayY[1] = -2 * imageData[i-1][j];
					arrayY[2] = -1 * imageData[i-1][j+1];
					arrayY[3] =  0 * imageData[i][j-1];
					arrayY[4] =  0 * imageData[i][j];
					arrayY[5] =  0 * imageData[i+1][j+1];
					arrayY[6] =  1 * imageData[i+1][j-1];
					arrayY[7] =  2 * imageData[i+1][j];
					arrayY[8] =  1 * imageData[i+1][j+1];
					
					int valorY = ler3x3(arrayY, 3);
					newImageDataY[i][j] = valorY;
				}
			}
			
			//Transformar o array de dados da imagem em imagem novamente
			theImage = new BufferedImage(maxWidth, maxHeight, image.getType());
			int value;
			for(int i = 0; i < maxWidth; i++){
			    for(int j = 0; j < maxHeight; j++){
			    	value = (int)Math.sqrt(Math.pow(newImageDataX[i][j], 2) + Math.pow(newImageDataY[i][j], 2));
			   	
			    	if(value < 0){
			    		value = 0;
			    	} else if(value > 255){
			    		value = 255;
			    	}
				    	
			    	Color c = new Color(value, value, value);
			    	int red = (int)(c.getRed());
			    	int green = (int)(c.getGreen());
			    	int blue = (int)(c.getBlue());
			    	c = new Color(red, green, blue);
			    	theImage.setRGB(i, j, c.getRGB());
			    }
			}
		} catch (Exception e) {
			msg.mensagem(9);
		}
		
		return gerarImagemFiltrada(theImage, 3);
	}


	/**
	 * Aplica o filtro de borda Prewitt na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return gerarImagemFiltrada() - Metodo que salva a imagem, nomeia com nome+Prewitt
	 * e retorna o nome da imagem salva.
	 */
	public String aplicarFiltroPrewitt(File file){
		int[][] imageData = getPictureFile(file);
		int[][] newImageDataX = getPictureFile(file);
		int[][] newImageDataY = getPictureFile(file);

		BufferedImage image;
		BufferedImage theImage = null;
		try {
			image = ImageIO.read(file);
			int maxHeight = image.getHeight(), maxWidth = image.getWidth(); //largura e altura da imagem
			
			for(int i = 1; i < maxWidth - 1; i++){
				for(int j = 1; j < maxHeight - 1; j++){
					/* le a vizinhanca de 8 e coloca todos os vizinhos em um array. 
					   Multiplica pela mascara padrao*/

					int[] arrayX = new int[9];
					arrayX[0] = -1 * imageData[i-1][j-1];
					arrayX[1] = -1 * imageData[i-1][j];
					arrayX[2] = -1 * imageData[i-1][j+1];
					arrayX[3] =  0 * imageData[i][j-1];
					arrayX[4] =  0 * imageData[i][j];
					arrayX[5] =  0 * imageData[i+1][j+1];
					arrayX[6] =  1 * imageData[i+1][j-1];
					arrayX[7] =  1 * imageData[i+1][j];
					arrayX[8] =  1 * imageData[i+1][j+1];		

					
					int valorX = ler3x3(arrayX, 3);
					newImageDataX[i][j] = valorX;
					
					int[] arrayY = new int[9];
					arrayY[0] = -1 * imageData[i-1][j-1];
					arrayY[1] =  0 * imageData[i-1][j];
					arrayY[2] =  1 * imageData[i-1][j+1];
					arrayY[3] = -1 * imageData[i][j-1];
					arrayY[4] =  0 * imageData[i][j];
					arrayY[5] =  1 * imageData[i+1][j+1];
					arrayY[6] = -1 * imageData[i+1][j-1];
					arrayY[7] =  0 * imageData[i+1][j];
					arrayY[8] =  1 * imageData[i+1][j+1];
					
					int valorY = ler3x3(arrayY, 3);
					newImageDataY[i][j] = valorY;
				}
			}
			
			//Transformar o array de dados da imagem em imagem novamente
			theImage = new BufferedImage(maxWidth, maxHeight, image.getType());
			int value;
			for(int i = 0; i < maxWidth; i++){
			    for(int j = 0; j < maxHeight; j++){
			    	value = (int)Math.sqrt(Math.pow(newImageDataX[i][j], 2) + Math.pow(newImageDataY[i][j], 2));
			    	
			    	if(value < 0){
			    		value = 0;
			    	} else if(value > 255){
			    		value = 255;
			    	}
				    	
			    	Color c = new Color(value, value, value);
			    	int red = (int)(c.getRed());
			    	int green = (int)(c.getGreen());
			    	int blue = (int)(c.getBlue());
			    	c = new Color(red, green, blue);
			    	theImage.setRGB(i, j, c.getRGB());
			    }
			}
		} catch (Exception e) {
			msg.mensagem(9);
		}
		
		return gerarImagemFiltrada(theImage, 33);
	}

	
	
	/**
	 * Aplica o filtro de borda Roberts na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return gerarImagemFiltrada() - Metodo que salva a imagem, nomeia com nome+Roberts  
	 * e retorna o nome da imagem salva.
	 */
	public String aplicarFiltroRoberts(File file){
		int[][] imageData = getPictureFile(file);
		int[][] newImageDataX = getPictureFile(file);
		int[][] newImageDataY = getPictureFile(file);

		BufferedImage image;
		BufferedImage theImage = null;
		try {
			image = ImageIO.read(file);
			int maxHeight = image.getHeight(), maxWidth = image.getWidth(); //largura e altura da imagem
			
			for(int i = 1; i < maxWidth - 1; i++){
				for(int j = 1; j < maxHeight - 1; j++){
					/* le a vizinhanca de 8 e coloca todos os vizinhos em um array. 
					   Multiplica pela mascara padrao*/

					int[] arrayX = new int[9];
					arrayX[0] =  0 * imageData[i-1][j-1];
					arrayX[1] =  0 * imageData[i-1][j];
					arrayX[2] = -1 * imageData[i-1][j+1];
					arrayX[3] =  0 * imageData[i][j-1];
					arrayX[4] =  1 * imageData[i][j];
					arrayX[5] =  0 * imageData[i+1][j+1];
					arrayX[6] =  0 * imageData[i+1][j-1];
					arrayX[7] =  0 * imageData[i+1][j];
					arrayX[8] =  0 * imageData[i+1][j+1];		

					
					int valorX = ler3x3(arrayX, 3);
					newImageDataX[i][j] = valorX;
					
					int[] arrayY = new int[9];
					arrayY[0] = -1 * imageData[i-1][j-1];
					arrayY[1] =  0 * imageData[i-1][j];
					arrayY[2] =  0 * imageData[i-1][j+1];
					arrayY[3] =  0 * imageData[i][j-1];
					arrayY[4] =  1 * imageData[i][j];
					arrayY[5] =  0 * imageData[i+1][j+1];
					arrayY[6] =  0 * imageData[i+1][j-1];
					arrayY[7] =  0 * imageData[i+1][j];
					arrayY[8] =  0 * imageData[i+1][j+1];
					
					int valorY = ler3x3(arrayY, 3);
					newImageDataY[i][j] = valorY;
				}
			}
			
			//Transformar o array de dados da imagem em imagem novamente
			theImage = new BufferedImage(maxWidth, maxHeight, image.getType());
			int value;
			for(int i = 0; i < maxWidth; i++){
			    for(int j = 0; j < maxHeight; j++){
			    	value = (int)Math.sqrt(Math.pow(newImageDataX[i][j], 2) + Math.pow(newImageDataY[i][j], 2));
			    	
			    	if(value < 0){
			    		value = 0;
			    	} else if(value > 255){
			    		value = 255;
			    	}
			    	
			    	Color c = new Color(value, value, value);
			    	int red = (int)(c.getRed());
			    	int green = (int)(c.getGreen());
			    	int blue = (int)(c.getBlue());
			    	c = new Color(red, green, blue);
			    	theImage.setRGB(i, j, c.getRGB());
			    }
			}
		} catch (Exception e) {
			msg.mensagem(9);
		}
		
		return gerarImagemFiltrada(theImage, 333);
	}	
	
	
	/**
	 * Le o array recebido por parametro, a opcao e retorna o valor obtido no calculo correspondente.
	 * @param array - Array de inteiros contendo os valores da vizinhanca de 8.
	 * @param opcao - Inteiro contendo a opcao de calculo a ser feito.
	 * 		1 = Maximo, 2 = Minimo, 3 = Borda, 4 = Mediana, 5 = Media, 6 = Binarizacao, 8 = Fourier.
	 * @return result - Inteiro contendo o valor obtido no calculo.
	 * @throws Exception
	 */
	private int ler3x3(int[] array, int opcao)throws Exception {
		int result = 0;
		
		switch (opcao) {
			case 1: //Maximo
				result = 0;
				Arrays.sort(array);
				return array[0];
			case 2://Minimo
				result = 0;
				Arrays.sort(array);
				return array[array.length-1];
			case 3: //Borda: Sobel, Prewitt e Roberts
				result = 0;
				for(int i = 0; i < array.length; i++){
					result += array[i];
				}
				return result;
			case 4: //Mediana
				Arrays.sort(array);
				return (array.length % 2 == 0)? array[array.length/2] : (array[array.length/2] + array[(array.length/2)+1])/2;
			case 5: //Media
				result = 0;
				for(int i = 0; i < array.length; i++){
					result += array[i];
				}
				result /= array.length;
				return result;
			case 6: //Binarizacao
				return -1;
			case 8: //Fourier
				return -1;
			default:
				return -1;
		}
	}

	
	/** 
	 * Define e preenche a mascara com um unico valor.
	 * @param celula - Inteiro que contem o valor de toda a mascara.
	 * @return mascara - Matriz de mascara.
	 */
	private int [][] mascara(int celula){
		
		int mascara[][] = new int [3][3];
		for(int i = 0; i < mascara.length; i++){
			for(int j = 0; j < mascara.length; j++){
				mascara[i][j] = celula;
			}
		}
		return mascara;		
	}

	
	/**
	 * Aplica o filtro de binarizacao na imagem.
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @param limiar - Inteiro que indica o limiar para a binarizacao.
	 * @return gerarImagemFiltrada() - Metodo que salva a imagem com nome+Binarizacao
	 * e retorna o nome da imagem salva.
	 */
	public String aplicarFiltroBinarizacao(File file, int limiar){
		int[][] imageData = getPictureFile(file);
		BufferedImage image;
		BufferedImage theImage = null;
		try {
			image = ImageIO.read(file);
			int maxHeight = image.getHeight(), maxWidth = image.getWidth(); //largura e altura da imagem

			int[][] newImageData = new int[maxWidth][maxHeight];
			
			for(int i = 0; i < maxWidth; i++){
				for(int j = 0; j < maxHeight; j++){
					if(imageData[i][j] < limiar){
						newImageData[i][j] = 0;
					} else {
						newImageData[i][j] = 255;
					}		
				}
			}
			
			//Transformar o array de dados da imagem em imagem novamente
			theImage = new BufferedImage(maxWidth, maxHeight, image.getType());
			int value = 0, cor = 0;
			for(int i = 0; i < maxWidth; i++){
			    for(int j = 0; j < maxHeight; j++){
			    	value = newImageData[i][j];
			    	Color c = new Color(value, value, value);
			    	cor = (int)(c.getRed());
			    	c = new Color(cor, cor, cor);
			    	theImage.setRGB(i, j, c.getRGB());
			    }
			}
		} catch (Exception e) {
			msg.mensagem(9);
		}
		
		return gerarImagemFiltrada(theImage, 6);
	}
	

	/**
	 * Aplicar negativo a imagem 
	 * @param file - Arquivo contendo a imagem a ser manipulada.
	 * @return gerarImagemFiltrada() - Metodo que salva a imagem, nomeia com nome+Negativo
	 * e retorna o nome da imagem salva. 
	 */
	
	public String aplicarNegativo(File file){
		BufferedImage image = null;
		
		try{
			image = ImageIO.read(file);
			
			int width = image.getWidth();
			int height = image.getHeight();
			for(int i = 0; i < width; i++) {
				for(int j = 0; j < height; j++) {
					int rgb = image.getRGB(i, j);               //a cor inversa é dado por 255 menos o valor da cor                 
					int r = 255 - (int)((rgb&0x00FF0000)>>>16);
					int g = 255 - (int)((rgb&0x0000FF00)>>>8);
					int b = 255 - (int) (rgb&0x000000FF);
					Color color = new Color(r, g, b);
					image.setRGB(i, j, color.getRGB());
				}
			}
		} catch (Exception e) {
			msg.mensagem(9);
		}
		
		return gerarImagemFiltrada(image, 9);
    }
	

	/**
	 * Metodo que salva a imagem filtrada com o nome de acordo com os filtros escolhidos.
	 * @param image - BufferedImage contendo imagem a ser salva.
	 * @param f - Caractere que indica o filtro que foi aplicado.
	 * @return nome - String contendo o nome da imagem de acordo com os filtros aplicados.
	 */
	public String gerarImagemFiltrada(BufferedImage image, int f) {	
		String nome = "";
		String filtro = "";
		
		switch (f){
			case 1:
				filtro += "Maximo";
				break;
			case 2:
				filtro += "Minimo";
				break;	
			case 3:
				filtro += "Sobel";
				break;	
			case 33:
				filtro += "Prewitt";
				break;
			case 333:
				filtro += "Roberts";
				break;
			case 4:
				filtro += "Mediana";
				break;	
			case 5:
				filtro += "Media";
				break;	
			case 6:
				filtro += "Binarizacao";
				break;	
			case 7:
				filtro += "Monocromatica";
				break;	
			case 8:
				filtro += "Fourier";
				break;	
			case 88:
				filtro += "Inversa";
				break;	
			case 9:
				filtro += "Negativo";
				break;
		}
		
		try {			
			nome = imagem.getName().replace(".jpg", "");
			nome += filtro + ".jpg";
			
			ImageIO.write(image, "JPG", new File(PATH + nome));
			
		} catch (IOException e) {
			msg.error(8);
			e.printStackTrace();
		}
		
		return nome;
	}
}
