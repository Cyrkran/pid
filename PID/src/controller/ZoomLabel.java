package controller;

import javax.swing.*;
import java.awt.Image;
import java.awt.Dimension;
import java.awt.Graphics;

/**
 * Classe retirada e adaptada do site: https://community.oracle.com/thread/1272085?start=15&tstart=0
 */
public class ZoomLabel extends JPanel {
    private Image img;
    private float scaleX, scaleY;

    
    /**
     * Construtor padrao da classe
     */
    public ZoomLabel() {
        scaleX = scaleY = 1;
        setOpaque(false);
    }
    
    
    /**
     * construtor que recebe um objeto do tipo Image
     * @param img imagem para avaliacao
     */
    public  ZoomLabel(Image img) {
         //quick and 'dirty' way to load an image
        this.img = new ImageIcon(img).getImage();
        scaleX = scaleY = 1;
        setOpaque(false);
    }
    
    
    /**
     * Construtor que recebe um objeto do tipo ImageIcon
     * @param icon icon para avaliacao
     */
    public ZoomLabel(ImageIcon icon) {
        this(icon.getImage());
    }
    
    
    /**
     * Seta uma imagem ao panel
     * @param img imagem a ser colocada no panel
     */
    public void setImage(Image img) {
        this.img = img;
        revalidate();
        repaint();
    }
    
    
    /**
     * seta uma nova escala � imagem
     * @param scaleX escala de X
     * @param scaleY escala de Y
     */
    public void setScale(float scaleX, float scaleY) {
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        revalidate();
        repaint();
    }
        

    @Override
    /**
     * Escolhe um valor de preferencia 
     */
    public Dimension getPreferredSize() {
        int prefWidth  = (int) (img == null?0:img.getWidth(this) * scaleX);
        int prefHeight = (int) (img == null?0:img.getHeight(this) * scaleY);

        return new Dimension(prefWidth,prefHeight);
    }
    
    
    @Override
    /**
     * pinta o componente no painel
     * @param g Objeto do tipo Graphics
     */
    public void paintComponent(Graphics g) {
        if(img == null)
            return;

        int w = (int) (img.getWidth(null) * scaleX);
        int h = (int) (img.getHeight(null) * scaleY);

        int x = (getWidth()-w)/2;
        int y = (getHeight()-h)/2;

        g.drawImage(img,x,y,w,h, null);
    }
}
