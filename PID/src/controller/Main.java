package controller;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import model.Mensagem;

public class Main {
	
	Mensagem msg = new Mensagem();
	
	/**
	 * Funcao que permite a escolha da imagem a ser manipulada.
	 * @return arquivo - Objeto do tipo File escolhido.
	 */
	public File escolherImagem(){
		
		File arquivo = null;
		JFileChooser chooser = new JFileChooser();  	
				
		String s = System.getProperty("user.dir") + System.getProperty("file.separator") + "bin" + 
										System.getProperty("file.separator") + "imagens" + System.getProperty("file.separator") + "base_de_imagens";
	       
		File pathInicial = new File(s);
        chooser.setCurrentDirectory(pathInicial); // definir o diretorio em que os arquivos serão escolhidos
        
	    FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG, GIF & PNG Images", "jpg", "gif", "png");  //Cria um filtro
	        chooser.setFileFilter(filter);  
	        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY); //Selecionar apenas um
	        int returnVal = chooser.showOpenDialog(null); 
	      try{
	        if(returnVal == JFileChooser.APPROVE_OPTION) {  //Verifica se o usuario clicou no botão OK        
	        	arquivo = chooser.getSelectedFile();
	        }
	     
	      } catch(Exception ex){
	    	  msg.error(4);
	      }
	        	
		return arquivo;
	}
}


