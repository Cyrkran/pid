package controller;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Metadados {
	
	BufferedWriter bw = null;
	String tipo;
	int[][] dados;
	
	public Metadados(String name, String tipo, int[][] dados){
		
		//Gera o arquivo de metadados da imagem
		try {
			//parametro true impede do FileWriter sobrescrever o que est� escrito no arquivo
			bw = new BufferedWriter(new FileWriter(new File(name + ".odt"), true));
			
			this.tipo = tipo;
			this.dados = dados;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public Metadados() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * Funcao que retorna a extensao do arquivo passado por parametro.
	 * @param f Objeto do tipo File.
	 * @return ext String extens�o do arquivo.
	 */
	public String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
	}


	/**
	 * Gera os arquivos com dados no caminho especificado
	 * @param file arquivo escolhido no comeco do programa
	 * @param ordenado array de double j� ordenado
	 */
	public void gerarMetadados(File file, double[][] ordenado) {
		try{
			
			bw = new BufferedWriter(new FileWriter(file));
			String data = "";
			for(int i = 0; i < 1000; i++){
				data += (int)ordenado[0][i] + " ";
			}
			bw.write(data);
			bw.flush();
			bw.close();
		}catch(Exception e){
			e.printStackTrace();
		}
			
	}


	/**
	 * Le o arquivo de metadados especifico
	 * @param file arquivo para ler 
	 * @return array de double com todas as informacoes necessarias para ordenacao
	 */
	public double[][] lerArquivo(File file) {
		// TODO Auto-generated method stub
		double[][] ordenado = new double[2][1000];
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			String[] array = br.readLine().split(" ");
			for(int i = 0; i < 1000; i++){
				ordenado[0][i] = Double.parseDouble(array[i]);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return ordenado;
	}
	
}
