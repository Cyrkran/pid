package controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import java.util.List;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;

import model.Histograma;
import model.Mensagem;

public class GerarHistograma extends Histograma {
	private static final int DEFAULT_WIDTH = 640;
	private static final int DEFAULT_HEIGHT = 410;
	private static final String DEFAULT_TITLE = "Histograma";
	private static final String DEFAULT_CATEGORY_LABEL = "Cores";
	private static final String DEFAULT_VALUE_LABEL = "Frequência";
	
	private final String PATH = "/imagens/base_de_imagens/";
	private final String PATH_HIST = "/imagens/histogramas/";
	File metaBasePath;

	
	Mensagem msg = new Mensagem();
	
	
	/**
	 * Construtor padrao.
	 */
	public GerarHistograma(){
		metaBasePath = new File(PATH_HIST); // caminho de onde os histogramas serão gerados

	}
	
	
	/**
	 * Metódo que gera o histograma de uma imagem especifica.
	 * @param nome - nome da imagem para gerar o histograma.
	 */
	public void gerarHistograma(String nome) {		
		BufferedImage imageReader;
		try {
			imageReader = ImageIO.read(getClass().getResource(PATH + nome));
			
			if(histogramaExiste(nome) == false){
				ChartUtilities.saveChartAsJPEG(new File(System.getProperty("user.dir") + System.getProperty("file.separator") + "bin" + 
						"" + System.getProperty("file.separator") + nome), 
				ChartFactory.createLineChart(DEFAULT_TITLE, DEFAULT_CATEGORY_LABEL, DEFAULT_VALUE_LABEL, getRGBDataset(imageReader)),
				DEFAULT_WIDTH, DEFAULT_HEIGHT);
			}
		} catch (IOException e) {
			msg.error(5);
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Confere se o histograma ja existe no diretorio.
	 * @param nome - nome do arquivo de histograma.
	 * @return <code>true</code> se o histograma existe <code>false</code> se não existe.
	 */
	public boolean histogramaExiste(String nome){
		boolean result = true;
		
		File file = new File(System.getProperty("user.dir") + System.getProperty("file.separator") + "bin" + 
				  System.getProperty("file.separator") + nome);

		if(!file.exists()){
			result = false;
		}
		
		return result;
	}
	
	/**
	 * Metodo que faz a media das distancias dos pixels para definir a distancia entre as imagens.
	 * @param imagem1 - Objeto do tipo BufferedImage que contem a primeira imagem a ser comparada. 
	 * @param imagem2 - Objeto do tipo BufferedImage que contem a segunda imagem a ser comparada.
	 * @return result - double resultante do calculo de media.
	 */
	public double distancia(BufferedImage imagem1, BufferedImage imagem2, String opcao, char d){
		double result = 0;
		
		double [] array = new double[256]; 
		
		array = compararHistogramas(imagem1, imagem2, opcao, d);
		
		for(int i = 0; i < array.length; i++){
			result += array[i];
		}
		
		result /= 256;
		
		return result;
	}
	
	
	/**
	 * Realiza a comparacao dos histogramas de duas imagens. 	
	 * @param imagem1 - Objeto do tipo BufferedImage que contem a primeira imagem a ser comparada. 
	 * @param imagem2 - Objeto do tipo BufferedImage que contem a segunda imagem a ser comparada.
	 * @param opcao - String com a referencia para o tipo de histograma a ser comparado.
	 * @return result - array de double contendo a distancia entre as frequencias de pixels das imagens.
	 */
	public double [] compararHistogramas(BufferedImage imagem1, BufferedImage imagem2, String opcao, char d){
		double [] result = new double [256];
		
		List<TreeMap<Integer, Integer>> hist1 = new ArrayList<TreeMap<Integer, Integer>>(3);
		List<TreeMap<Integer, Integer>> hist2 = new ArrayList<TreeMap<Integer, Integer>>(3);

		if(opcao.equals("RGB")){
			hist1 = getRGBValues(imagem1);
			hist2 = getRGBValues(imagem2);
		}else if(opcao.equals("YUV")){
			hist1 = getYUVValues(imagem1);
			hist2 = getYUVValues(imagem2);
		}else if(opcao.equals("HSV")){
			hist1 = getHSVValues(imagem1);
			hist2 = getHSVValues(imagem2);
		}else if(opcao.equals("RGBN")){
			hist1 = getRGBNValues(imagem1);
			hist2 = getRGBNValues(imagem2);
		}else if(opcao.equals("YUVN")){
			hist1 = getYUVNValues(imagem1);
			hist2 = getYUVNValues(imagem2);
		}else if(opcao.equals("HSVN")){
			hist1 = getHSVNValues(imagem1);
			hist2 = getHSVNValues(imagem2);
		}
		
		switch(d){
			case 'c':
				result = gerarArrayDistanciaCosseno(hist1, hist2);
				break;
			case 'e':
				result = gerarArrayDistanciaEuclidiana(hist1, hist2);
				break;
			case 'm':
				result = gerarArrayDistanciaManhattan(hist1, hist2);
				break;
			case 'x':
				result = gerarArrayDistanciaXadrez(hist1, hist2);
				break;
				
		}
		
		return result;	
	}
	
	
	/**
	 * Metodo que realiza a comparacao entre dois objetos List e o calculo de distancia entre os pixels.
	 * @param list1 - Objeto do tipo List que contem os valores de histograma da primeira imagem a ser comparada.
	 * @param list2 - Objeto do tipo List que contem os valores de histograma da segunda imagem a ser comparada.
	 * @return distancia - Array de double contendo a distancia entre os valores de frequencia de cor das imagens.
	 */
	public double [] gerarArrayDistanciaCosseno(List<TreeMap<Integer, Integer>> list1, List<TreeMap<Integer, Integer>> list2){
		double [] distancia = new double [256];
		
		int [][] imagem1 = new int [3][256];
		int [][] imagem2 = new int [3][256];

		imagem1 = gerarArrayHistograma(list1);
		imagem2 = gerarArrayHistograma(list2);

		Similaridade s = new Similaridade();
		
		for(int i = 0; i < 256; i++){			
			distancia[i] = s.cosseno(imagem1, imagem2);
		}
		
		return distancia;
	}
	
	
	/**
	 * Metodo que realiza a comparacao entre dois objetos List e o calculo de distancia entre os pixels.
	 * @param list1 - Objeto do tipo List que contem os valores de histograma da primeira imagem a ser comparada.
	 * @param list2 - Objeto do tipo List que contem os valores de histograma da segunda imagem a ser comparada.
	 * @return distancia - Array de double contendo a distancia entre os valores de frequencia de cor das imagens.
	 */
	public double [] gerarArrayDistanciaEuclidiana(List<TreeMap<Integer, Integer>> list1, List<TreeMap<Integer, Integer>> list2){
		double [] distancia = new double [256];
		
		int [][] imagem1 = new int [3][256];
		int [][] imagem2 = new int [3][256];

		imagem1 = gerarArrayHistograma(list1);
		imagem2 = gerarArrayHistograma(list2);

		Similaridade s = new Similaridade();
		
		for(int i = 0; i < 256; i++){
			distancia[i] = s.euclidiana(imagem1, imagem2);
		}
		
		return distancia;
	}
	
	
	/**
	 * Metodo que realiza a comparacao entre dois objetos List e o calculo de distancia entre os pixels.
	 * @param list1 - Objeto do tipo List que contem os valores de histograma da primeira imagem a ser comparada.
	 * @param list2 - Objeto do tipo List que contem os valores de histograma da segunda imagem a ser comparada.
	 * @return distancia - Array de double contendo a distancia entre os valores de frequencia de cor das imagens.
	 */
	public double [] gerarArrayDistanciaManhattan(List<TreeMap<Integer, Integer>> list1, List<TreeMap<Integer, Integer>> list2){
		double [] distancia = new double [256];
		
		int [][] imagem1 = new int [3][256];
		int [][] imagem2 = new int [3][256];

		imagem1 = gerarArrayHistograma(list1);
		imagem2 = gerarArrayHistograma(list2);

		Similaridade s = new Similaridade();
		
		for(int i = 0; i < 256; i++){			
			//distancia unidimensional
			distancia[i] = s.manhattan(imagem1, imagem2);
		}
		
		return distancia;
	}
	
	
	/**
	 * Metodo que realiza a comparacao entre dois objetos List e o calculo de distancia entre os pixels.
	 * @param list1 - Objeto do tipo List que contem os valores de histograma da primeira imagem a ser comparada.
	 * @param list2 - Objeto do tipo List que contem os valores de histograma da segunda imagem a ser comparada.
	 * @return distancia - Array de double contendo a distancia entre os valores de frequencia de cor das imagens.
	 */
	public double [] gerarArrayDistanciaXadrez(List<TreeMap<Integer, Integer>> list1, List<TreeMap<Integer, Integer>> list2){
		double [] distancia = new double [256];
		
		int [][] imagem1 = new int [3][256];
		int [][] imagem2 = new int [3][256];

		imagem1 = gerarArrayHistograma(list1);
		imagem2 = gerarArrayHistograma(list2);

		Similaridade s = new Similaridade();
		
		for(int i = 0; i < 256; i++){			
			//distancia unidimensional
			distancia[i] = s.xadrez(imagem1, imagem2);
		}
		
		return distancia;
	}
	
	
	
	/**
	 * Transforma o objeto List em uma matriz que contem o histograma RGB. 
	 * Linha 0 - Red.
	 * Linha 1 - Green.
	 * Linha 2 - Blue.
	 * @param list - Objeto do tipo List a ser manipulado.
	 * @return matrizHistograma - Matriz de inteiros resultante do processamento.
	 */
	public int [][] gerarArrayHistograma(List<TreeMap<Integer, Integer>> list){
		int [][] mediaArrayHistograma = new int [3][256];

		for(int i = 0; i < 256; i++){
			if(list.get(0).get(i) != null){
				mediaArrayHistograma[0][i] = list.get(0).get(i);
			}
		}
			
		for(int i = 0; i < 256; i++){
			if(list.get(1).get(i) != null){
				mediaArrayHistograma[1][i] = list.get(1).get(i);
			}			
		}

		for(int i = 0; i < 256; i++){
			if(list.get(2).get(i) != null){
				mediaArrayHistograma[2][i] = list.get(2).get(i);
			}			
		}
		
		return mediaArrayHistograma;
	}	
}//end class
