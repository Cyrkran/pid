package controller;

import javax.swing.JOptionPane;

public class Similaridade {
	
	public double cosseno(int [][] i1, int [][] i2){
		double produto = 0, soma1 = 0, soma2 = 0, distancia = 0;
		
		for(int i = 0; i < i1.length; i++){	
	        produto += (i1[0][i] * i2[0][i]) + (i1[1][i] * i2[1][i]) + (i1[2][i] * i2[2][i]);
	        soma1 += Math.pow(i1[0][i],2) + Math.pow(i1[1][i],2) + Math.pow(i1[2][i],2);
	        soma2 += Math.pow(i2[0][i],2) + Math.pow(i2[1][i],2) + Math.pow(i2[2][i],2);
		}
		
		distancia = produto/(Math.sqrt(soma1) * Math.sqrt(soma2));	
		
		return distancia;
	}

	public double euclidiana(int  [][] i1, int [][] i2){
		double aux, distancia = 0;
		
		for(int i = 0; i < i1.length; i++){	
			aux = Math.pow(i1[0][i] - i2[0][i], 2) + Math.pow(i1[1][i] - i2[1][i], 2) + Math.pow(i1[2][i] - i2[2][i], 2);
			distancia += aux; 
		}

		distancia = Math.sqrt(distancia);
		
		return distancia;
	}
	
	public double manhattan(int [][] i1, int [][] i2){
		double distancia = 0;
		
		for(int i = 0; i < i1.length; i++){	
			distancia += Math.abs(i1[0][i] - i2[0][i]) + Math.abs(i1[1][i] - i2[1][i]) + Math.abs(i1[2][i] - i2[2][i]);
		}
				
		return distancia;
	}
	
	public double xadrez(int [][] i1, int [][] i2){
		double distancia = 0;
		double maximo = 0;
		
		for(int i = 0; i < i1.length; i++){	
			maximo = Math.max(Math.abs(i1[0][i] - i2[0][i]), Math.abs(i1[1][i] - i2[1][i]));
			maximo = Math.max(maximo, Math.abs(i1[2][i] - i2[2][i]));
			distancia += maximo;
		}
		
		return distancia;
	}
	

	

}
