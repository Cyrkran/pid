package controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.jfree.data.category.DefaultCategoryDataset;

public class PrecisaoRevocacao {
	BufferedImage imagem;
	String nome;
	File file = null;
	int relevantes = 0;
	//int recuperados;
	int numComponentes = 0;
	
	double revocacao = 0;
	double precisao = 0;
	double[][] matriz = null;
	int max = 0;
	String[] nomes = null;
	

	public PrecisaoRevocacao(File file, int numComponentes, String[] nomes){
		try {
			imagem = ImageIO.read(file);
			nome = file.getName().split(".jpg")[0];
			this.file = file;
			this.numComponentes = numComponentes;
			this.nomes = nomes;
			getMax(nome);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void getMax(String nome) {
		// TODO Auto-generated method stub
		int nomeArquivo = Integer.parseInt(nome);
		if(nomeArquivo <= 99){
			this.max = 99;
		}else if(nomeArquivo > 99 && nomeArquivo <= 199){
			this.max = 199;
		}else if(nomeArquivo > 199 && nomeArquivo <= 299){
			this.max = 299;
		}else if(nomeArquivo > 299 && nomeArquivo <= 399){
			this.max = 399;
		}else if(nomeArquivo > 399 && nomeArquivo <= 499){
			this.max = 499;
		}else if(nomeArquivo > 499 && nomeArquivo <= 599){
			this.max = 599;
		}else if(nomeArquivo > 599 && nomeArquivo <= 699){
			this.max = 699;
		}else if(nomeArquivo > 699 && nomeArquivo <= 799){
			this.max = 799;
		}else if(nomeArquivo > 799 && nomeArquivo <= 899){
			this.max = 899;
		}else if(nomeArquivo > 899 && nomeArquivo <= 999){
			this.max = 999;
		}
	}


	
	/**
	 * Calcula os valores da precisar e revocacao dos elementos na lista
	 */
	public void calcularPrecisaoRevocacao(){
		double[][] ordenado = new double[2][1000];
		Metadados mt = new Metadados();
		//ordenado = mt.lerArquivo(file);
		matriz = new double[numComponentes][numComponentes];
		for(int i = 0; i < numComponentes; i++){
			if(Integer.parseInt(nomes[i].split(".jpg")[0]) > (max-100) && Integer.parseInt(nomes[i].split(".jpg")[0]) <= max){
				System.out.println(relevantes);
				relevantes++;
			}
			
			precisao = relevantes/(i+1.0);
			revocacao = (relevantes/100.0);
			System.out.println(precisao + "\t" + revocacao);
			matriz[i][0] = revocacao;
			matriz[i][1] = precisao;
		}		
	}
	
	
	/**
	 * Gerar grafico precisao x revocacao.
	 * @return dataset - Conjunto de dados.
	 */
	public DefaultCategoryDataset plotarGrafico(){
		calcularPrecisaoRevocacao();
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		for(int i = 0; i < numComponentes; i++){
			//System.out.println(revocacao + "\t" + precisao);

			dataset.addValue(matriz[i][0], "Precisao", matriz[i][1]+"");
		}
		/*dataset.addValue(4, "Precisao", "1");
		dataset.addValue(3, "Precisao", "3");
		dataset.addValue(5, "Precisao", "7");
		dataset.addValue(7, "Precisao", "3");
		dataset.addValue(20, "Precisao", "15");*/
		
		return dataset;
	}
}
